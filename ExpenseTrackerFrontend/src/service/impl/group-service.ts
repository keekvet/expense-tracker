import { GroupService } from '../interfaces/group-service'
import api from './api-service'
import { Group } from '_/model/group/types'
import { User } from '_/model/user/types'

const GroupServiceImpl: GroupService = {
    create,
    update,
    getById,
    delete: _delete,
    getAll,
    addUserToGroup,
    deleteUserFromGroup,
    getUsersByGroupId, // from user dto
}

export default GroupServiceImpl

function create(name: string) {
    return api.post<Group>(['groups'], { name })
}

function update(data: { id: number, name: string }) {
    return api.put<void>(['groups'], data)
}

function getById(id: number) {
    return api.get<Group>(['groups', id])
}

function _delete(id: number) {
    return api.delete<void>(['groups', id])
}

function getAll(userId: number) {
    return api.get<Group[]>(['users', userId, 'groups'])
}

function addUserToGroup(groupId: number, userId: number) {
    return api.post<void>(['groups', groupId, 'users', userId])
}

function deleteUserFromGroup(groupId: number, userId: number) {
    return api.delete<void>(['groups', groupId, 'users', userId])
}

function getUsersByGroupId(groupId: number) {
    return api.get<User[]>(['groups', groupId, 'users'])
}
