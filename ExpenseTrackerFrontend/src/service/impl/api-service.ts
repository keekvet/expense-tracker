import axios, { AxiosResponse } from 'axios'
import { store } from '_/facade/redux/store'
import { Api, UserUrl } from '../interfaces/api-service'

const instance = axios.create({
    baseURL: 'https://localhost:44341/api/',
    timeout: 5_000,
})

instance.interceptors.request.use(config => {
    if (config.headers)
        config.headers['Authorization'] = `Bearer ${store.getState().auth.token}`
    return config
})

const responseBody = <T>(response: AxiosResponse<T>) => response.data

const api: Api = {
    get: <T>(url: UserUrl) => instance.get<T>(convertUserUrl(url)).then(responseBody),
    post: <T>(url: UserUrl, body?: {}) => instance.post<T>(convertUserUrl(url), body).then(responseBody),
    put: <T>(url: UserUrl, body?: {}) => instance.put<T>(convertUserUrl(url), body).then(responseBody),
    delete: <T>(url: UserUrl) => instance.delete<T>(convertUserUrl(url)).then(responseBody)
}

function convertUserUrl(url: UserUrl) {
    return url.join('/')
}

export default api
