import api from './api-service'
import { ExpenseService } from '../interfaces/expense-service'
import { CreateExpense, Expense } from '_/model/expense/types'
import { UserLoan } from '_/model/expense/split-type'

const ExpenseServiceImpl: ExpenseService = {
    create,
    update,
    getById,
    delete: _delete,
    getExpensesByUserId,
    getExpensesByGroupId,
    getLoansByExpenseId,
    getUserLoanByExpenseId
}

export default ExpenseServiceImpl

function create(data: CreateExpense) {
    return api.post<Expense>(['expenses'], data)
}

function update(data: { id: number, name: string }) {
    return api.put<void>(['expenses'], data)
}

function getById(id: number) {
    return api.get<Expense>(['expenses', id])
}

function _delete(id: number) {
    return api.delete<void>(['expenses', id])
}

function getExpensesByUserId(userId: number) {
    return api.get<Expense[]>(['users', userId, 'expenses'])
}

function getExpensesByGroupId(groupId: number) {
    return api.get<Expense[]>(['groups', groupId, 'expenses'])
}

function getLoansByExpenseId(expenseId: number) {
    return api.get<UserLoan[]>(['expenses', expenseId, 'loans'])
}

function getUserLoanByExpenseId(expenseId: number, userId: number) {
    return api.get<number>(['expenses', expenseId, 'users', userId, 'loans'])
}
