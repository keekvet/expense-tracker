import { Login, LoginResponse, Registration } from '_/model/auth/types'
import { AuthServise } from '../interfaces/auth-service'
import api from './api-service'

export const AuthServiseImpl: AuthServise = {
    login,
    register,
}

export default AuthServiseImpl

function login(body: Login) {
    return api.post<LoginResponse>(['login'], body)
}

function register(body: Registration) {
    return api.post<void>(['registration'], body)
}