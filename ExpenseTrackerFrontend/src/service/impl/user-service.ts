import { User } from '_/model/user/types'
import { UserService } from '../interfaces/user-service'
import api from './api-service'

const UserServiceImpl: UserService = {
    update,
    getById,
    delete: _delete,
    updatePassword,
}

export default UserServiceImpl

function update(data: User) {
    return api.put<void>(['users'], data)
}

function getById(id: number) {
    return api.get<User>(['users', id])
}

function _delete(id: number) {
    return api.delete<void>(['users', id])
}

function updatePassword(userId: number, data: { oldPassword: string, newPassword: string }) {
    return api.put<void>(['users', userId, 'password'], data)
}
