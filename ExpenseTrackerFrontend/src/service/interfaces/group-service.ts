import { Group } from '_/model/group/types'
import { User } from '_/model/user/types'

interface GroupService {
    create: (name: string) => Promise<Group>
    update: (data: { id: number, name: string }) => Promise<void>
    getById: (id: number) => Promise<Group>
    delete: (id: number) => Promise<void>
    getAll: (userId: number) => Promise<Group[]>
    addUserToGroup: (groupId: number, userId: number) => Promise<void>
    deleteUserFromGroup: (groupId: number, userId: number) => Promise<void>
    getUsersByGroupId: (groupId: number) => Promise<User[]> // from user dto
}

export {
    type GroupService,
}
