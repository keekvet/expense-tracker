import { Login, LoginResponse, Registration } from '_/model/auth/types'

interface AuthServise {
    login: (data: Login) => Promise<LoginResponse>
    register: (data: Registration) => Promise<void>
}

export {
    type AuthServise,
}