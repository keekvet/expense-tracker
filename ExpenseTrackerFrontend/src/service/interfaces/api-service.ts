type UserUrl = (string | number)[]

interface Api {
    get: <T>(url: UserUrl) => Promise<T>
    post: <T>(url: UserUrl, body?: {}) => Promise<T>
    put: <T>(url: UserUrl, body?: {}) => Promise<T>
    delete: <T>(url: UserUrl) => Promise<T>
}

export {
    type UserUrl,
    type Api,
}