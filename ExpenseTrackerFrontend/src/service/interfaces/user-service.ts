import { User } from '_/model/user/types'

interface UserService {
    getById: (id: number) => Promise<User>
    delete: (id: number) => Promise<void>
    update: (data: User) => Promise<void>
    updatePassword: (userId: number, data: { oldPassword: string, newPassword: string }) => Promise<void>
}

export {
    type UserService,
}
