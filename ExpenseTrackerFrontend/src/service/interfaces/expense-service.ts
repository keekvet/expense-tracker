import { UserLoan } from '_/model/expense/split-type'
import { CreateExpense, Expense, UpdateExpense } from '_/model/expense/types'

interface ExpenseService {
    create: (data: CreateExpense) => Promise<Expense>
    update: (data: UpdateExpense) => Promise<void>
    getById: (id: number) => Promise<Expense>
    delete: (id: number) => Promise<void>
    getExpensesByUserId: (userId: number) => Promise<Expense[]>
    getExpensesByGroupId: (groupId: number) => Promise<Expense[]>
    getLoansByExpenseId: (expenseId: number) => Promise<UserLoan[]>
    getUserLoanByExpenseId: (expenseId: number, userId: number) => Promise<number>
}

export {
    type ExpenseService,
}
