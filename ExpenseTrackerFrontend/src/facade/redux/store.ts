import { combineReducers, configureStore } from '@reduxjs/toolkit'
import { authReducer } from '_/features/auth/reducer'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import { toastsReducer } from '_/features/toast/reducer'

const rootReducer = combineReducers({
    auth: authReducer,
    toasts: toastsReducer,
})

const persistConfig = {
    key: 'root',
    storage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
    reducer: persistedReducer
})

export const persist = persistStore(store)

export type RootState = ReturnType<typeof rootReducer>
export type AppStore = typeof store
export type AppDispatch = AppStore['dispatch']
