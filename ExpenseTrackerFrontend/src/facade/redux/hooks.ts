import { AsyncThunk } from '@reduxjs/toolkit'
import { useCallback } from 'react'
import { TypedUseSelectorHook,
    useDispatch as useDefDispatch, useSelector as useDefSelector } from 'react-redux'
import { unwrapResult } from '@reduxjs/toolkit'
import { toast } from '_/features/toast/effects'
import { logout } from '_/features/auth/effects'
import { AppDispatch, RootState } from './store'

export const useDispatch = () => useDefDispatch<AppDispatch>()
export const useSelector: TypedUseSelectorHook<RootState> = useDefSelector

export function useAction<P extends any, R extends any>(action: AsyncThunk<R, P, any>) {
    const dispatch = useDispatch()

    return useCallback(
        (arg: P) =>
            dispatch(action(arg))
                .then(unwrapResult)
                .catch(err => {
                    const status = getStatusCodeFromMessage(err.message)
                    if (status && status === 401)
                        dispatch(logout())

                    err.name === 'Error'
                        ? dispatch(toast({ type: 'error', message: err.message }))
                        : dispatch(toast({ type: 'warning', message: err.message }))
                    return Promise.reject(err)
                }),
        [dispatch]
    )
}

function getStatusCodeFromMessage(message: string) {
    const match = message.match(/\d+/)
    return match ? +match.join('') : null
}
