import { Routes, Route, Navigate, useLocation } from 'react-router-dom'
import { useSelector } from './facade/redux/hooks'
import routes from '_/constants/routes'
import Layout from './pages/layout/layout'
import LoginPage from './pages/auth/login-page'
import RegisterPage from './pages/auth/register-page'
import { useRouteGuard } from './features/navigation/hooks'
import AppRoutes from './features/navigation/app-routes'

function App() {
    const currentUser = useSelector(_ => _.auth.currentUser)
        , pathname = useLocation().pathname
        , bgColor = getBackgroungColor(pathname)

    useRouteGuard()

    return ( //bg-slate-100 //bg-rose-300 //bg-purple-300 //bg-red-100
        <div className={`h-screen flex p-14 selection:bg-purple-300 selection:text-black ${bgColor}`}>
            {currentUser
                ? <Layout children={<AppRoutes />} />
                : <Routes>
                    <Route path={routes.LOGIN} element={<LoginPage />} />
                    <Route path={routes.REGISTER} element={<RegisterPage />} />
                    <Route path='*' element={<Navigate to={routes.LOGIN} />} />
                </Routes>
            }
        </div>
    )
}

export default App

function getBackgroungColor(route: string) {
    switch(route) {
        case routes.LOGIN: return 'bg-purple-300'
        case routes.REGISTER: return 'bg-red-300'
        default: return 'bg-stone-500/20'
    }
}
