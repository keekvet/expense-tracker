export const CREATE_GROUP = { key: 'group', value: 'create' }

import * as modal_params from './modal-params'

type ModalParam = typeof CREATE_GROUP

export {
    modal_params as default,
    type ModalParam,
}
