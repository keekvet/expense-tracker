export const EMPTY = '/'
           , LOGIN = '/login'
           , REGISTER = '/register'
           , HOME = '/home'

           , GROUPS = '/groups'
           , GROUP = '/groups/:id'

           , SETTINGS = '/settings'
           , ERROR = '/error'

import * as routes from './routes'

type Route =
    | typeof EMPTY
    | typeof LOGIN
    | typeof REGISTER
    | typeof HOME
    | typeof GROUPS
    | typeof GROUP
    | typeof SETTINGS
    | typeof ERROR

export {
    routes as default,
    type Route,
}
