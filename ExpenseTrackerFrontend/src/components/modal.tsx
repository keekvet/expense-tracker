import { ReactNode, useRef } from 'react'

interface Props {
    children: ReactNode
    onClose: () => void
}

function Modal(props: Props) {
    const modalContainerRef = useRef<HTMLDivElement>(null)

    function handleContainerClick(e: any) {
        if (e.target === modalContainerRef.current)
            props.onClose()
    }

    return (
        <div
            className='fixed top-0 left-0 bottom-0 right-0 flex items-center justify-center bg-gray-800/70' // backdrop-blur-xl
            ref={modalContainerRef}
            onClick={handleContainerClick}
        >
            <div className='bg-white border-2 border-black rounded-2xl px-6 py-3 pb-6'>
                <div className='flex justify-end'>
                    <span className='font-semibold text-gray-600 cursor-pointer' onClick={props.onClose}>Close</span>
                </div>
                {props.children}
            </div>
        </div>
    )
}

export default Modal
