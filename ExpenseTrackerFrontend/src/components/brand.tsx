interface Props {
    className?: string
}

function Brand(props: Props) {
    return (
        <div
            className={`text-6xl font-bold text-blue-600 ${props.className ? props.className : ''}`}
        >
            divie.
        </div>
    )
}

export default Brand