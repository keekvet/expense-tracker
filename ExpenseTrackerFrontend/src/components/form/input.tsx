import { InputHTMLAttributes } from 'react';
import { Control, Controller } from 'react-hook-form';
import { omit } from '_/utils/object';

interface OwnProps {
    name: string
    label?: string
    control: Control<any, object>
    containerClassName?: string
}

type Props = OwnProps & InputHTMLAttributes<HTMLInputElement>

function Input(props: Props) {
    const { name, label, control, className, containerClassName } = props
        , inputProps = omit(props, 'className', 'name', 'label', 'control', 'containerClassName')

    return (
        <Controller
            name={name}
            control={control}
            render={({ field, fieldState: {error} }) => (
                <div className={`flex flex-col mb-2 ${containerClassName}`}>
                    {label && <label className=' text-sm font-medium mb-1'>{label}</label>}
                    <input className={`px-4 py-1 mb-1 border-2 border-black rounded-xl font-medium focus:outline-none focus:ring-1 ${error ? 'focus:border-pink-700 focus:ring-pink-500' : 'focus:border-blue-600 focus:ring-blue-500' } ${className}`} {...inputProps} {...field} />
                    {error && <p className='text-pink-700 text-sm font-medium'>{error.message}</p>}
                </div>
            )}
        />
    )
}

export default Input
