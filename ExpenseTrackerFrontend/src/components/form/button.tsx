import { omit } from '_/utils/object'

type Props = React.ButtonHTMLAttributes<HTMLButtonElement>

function Button(props: Props) {
    return (
        <button
            {...omit(props, 'className')}
            className={`btn ${props.className}`}
        />
    )
}

export default Button