interface ChangePassword {
    oldPassword: string
    newPassword: string
    newPasswordConfirmation: string
}

export {
    type ChangePassword,
}