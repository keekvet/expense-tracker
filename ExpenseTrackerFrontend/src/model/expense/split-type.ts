export const ABSOLUTE = 0
           , PERSENT = 1

// import * as split_type from './split-type'

type SplitType = typeof ABSOLUTE | typeof PERSENT

type EnumListType = { id: SplitType, name: string }[]

const EnumList: EnumListType = [
    {
        id: ABSOLUTE,
        name: 'Absolute'
    },
    {
        id: PERSENT,
        name: 'Persent'
    },
]

interface UserLoan {
    userId: number
    moneyDifference: number
}

export {
    type SplitType,
    EnumList,
    type UserLoan,
}
