import { SplitType } from './split-type'

interface CreateExpense {
    name: string
    groupId: number
    splitType: SplitType
}

interface Expense extends CreateExpense {
    id: number
    isActive: boolean
    date: string // TODO: Specify type
}

type UpdateExpense = Pick<Expense, 'id' | 'name' | 'splitType'>

export {
    type Expense,
    type CreateExpense,
    type UpdateExpense,
}
