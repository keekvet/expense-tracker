import { User } from '../user/types'

interface Login {
    userName: string
    password: string
}

interface LoginResponse {
    token: string,
    user: User
}

interface Registration extends Login {
    passwordConfirmation: string
}

export {
    type Login,
    type LoginResponse,
    type Registration,
}