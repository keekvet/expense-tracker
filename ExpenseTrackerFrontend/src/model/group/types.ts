import { User } from '../user/types'

interface Group {
    id: number
    name: string
    users: User[]
}

export {
    type Group,
}
