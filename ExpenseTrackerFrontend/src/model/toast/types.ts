interface Toast {
    id: string,
    type: 'warning' | 'error' | 'info' | 'success',
    message: string
}

export {
    type Toast,
}