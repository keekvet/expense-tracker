import React from 'react'
import ReactDOM from 'react-dom'
import './index.scss'
import { BrowserRouter as Router } from 'react-router-dom'
import { Provider } from 'react-redux'
import { store, persist } from './facade/redux/store'
import { PersistGate } from 'redux-persist/es/integration/react'
import App from './App'
import ToastsSpy from './features/toast/toasts-spy'

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <PersistGate persistor={persist} loading={null}>
                <Router>
                    <ToastsSpy />
                    <App />
                </Router>
            </PersistGate>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
)
