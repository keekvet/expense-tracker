import { getThunkCreator } from '_/utils/redux'
import api from '_/service/impl/expense-service'

const thunk = getThunkCreator('expense')

export const createExpense = thunk('createExpense', api.create)

export const updateExpense = thunk('updateExpense', api.update)

export const getExpense = thunk('getExpense', ({ id }: { id: number }) => api.getById(id))

export const deleteGroup = thunk('deleteExpense', ({ id }: { id: number }) => api.delete(id))

export const getExpensesByUserId = thunk(
    'getExpensesByUserId',
    ({ userId }: { userId: number }) =>
        api.getExpensesByUserId(userId)
)

export const getExpensesByGroupId = thunk(
    'getExpensesByGroupId',
    ({ groupId }: { groupId: number }) =>
        api.getExpensesByGroupId(groupId)
)

export const getLoansByExpenseId = thunk(
    'getLoansByExpenseId',
    ({ expenseId }: { expenseId: number }) =>
        api.getLoansByExpenseId(expenseId)
)

export const getUserLoanByExpenseId = thunk(
    'getUserLoanByExpenseId',
    ({ expenseId, userId }: { expenseId: number, userId: number }) =>
        api.getUserLoanByExpenseId(expenseId, userId)
)
