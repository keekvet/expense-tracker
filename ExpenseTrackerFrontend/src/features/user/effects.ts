import { createAsyncThunk } from '@reduxjs/toolkit'
import { User } from '_/model/user/types'
import api from '_/service/impl/user-service'
import { authActions } from '../auth/reducer'

export const getUser = createAsyncThunk(
    'user/getUser',
    ({ id }: { id: number }) => api.getById(id)
)

export const deleteUser = createAsyncThunk(
    'user/deleteUser',
    ({ id }: { id: number }) => api.delete(id)
)

export const updateUsername = createAsyncThunk(
    'user/updateUsername',
    (data: User, { dispatch }) =>
        api.update(data)
            .then(_ => dispatch(authActions.userChangedName(data.name)))
)

export const updatePassword = createAsyncThunk(
    'user/updatePassword',
    ({ userId, oldPassword, newPassword }: { userId: number, oldPassword: string, newPassword: string }) =>
        api.updatePassword(userId, { oldPassword, newPassword })
)
