import Brand from '_/components/brand'
import Button from '_/components/form/button'
import routes from '_/constants/routes'
import { useAction } from '_/facade/redux/hooks'
import * as authEffects from '_/features/auth/effects'
import { HiOutlineLogout } from 'react-icons/hi'
import Navlink from '_/features/navigation/navlink'

function Navbar() {
    const logout = useAction(authEffects.logout)

    return (
        <div className='self-stretch flex-shrink-0 flex-grow-0 basis-1/4 mirror-shadow before:bg-purple-500'>
            <nav className='h-full py-6 px-2 overflow-hidden bg-purple-300 border-2 border-black rounded-2xl'>
                <div className='text-center mb-4'>
                    <Brand className='text-black text-2xl' />
                </div>
                <ul>
                    <li className='mb-0.5'>
                        <Navlink route={routes.HOME} />
                    </li>
                    <li className='mb-0.5'>
                        <Navlink route={routes.GROUPS} />
                    </li>
                    <li className='mb-0.5'>
                        <Navlink route={routes.SETTINGS} />
                    </li>
                    <li className='mt-6'>
                        <Button className='p-0 w-full bg-yellow-300' onClick={() => logout()}>
                            <div className='flex px-2 py-1'>
                                <HiOutlineLogout size={16} className='relative top-0.5' />
                                <span className='ml-1 font-medium text-base lowercase'>Log out</span>
                            </div>
                        </Button>
                    </li>
                </ul>
            </nav>
        </div>

    )
}

export default Navbar
