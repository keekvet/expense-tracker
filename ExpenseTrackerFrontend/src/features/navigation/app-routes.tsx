import routes from '_/constants/routes'
import { Route, Routes } from 'react-router-dom'
import HomePage from '_/pages/home/home-page'
import GroupsPage from '_/pages/groups/groups-page'
import GroupPage from '_/pages/groups/group-page'
import ErrorPage from '_/pages/error/error-page'
import SettingsPage from '_/pages/settings/settings-page'

function AppRoutes() {
    return (
        <Routes>
            <Route path={routes.HOME} element={<HomePage />} />
            <Route path={routes.GROUPS} element={<GroupsPage />} />
            <Route path={routes.GROUP} element={<GroupPage />} />
            <Route path={routes.SETTINGS} element={<SettingsPage />} />
            <Route path='*' element={<ErrorPage />} />
        </Routes>
    )
}

export default AppRoutes
