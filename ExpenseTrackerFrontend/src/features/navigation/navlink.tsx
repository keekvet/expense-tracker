import { AiFillHome } from 'react-icons/ai'
import { BsFillPeopleFill } from 'react-icons/bs'
import { RiSettings4Fill } from 'react-icons/ri'
import { Link, useLocation } from 'react-router-dom'
import routes, { Route } from '_/constants/routes'

interface Props {
    route: Route
}

function Navlink({ route }: Props) {
    const pathname = useLocation().pathname
        , Icon = getNavlinkIcon(route)

    return (
        <Link to={route}>
            <div className={`flex px-2 py-1 lowercase font-medium border-2 rounded-xl ${pathname === route ? 'border-black bg-blue-700' : 'border-transparent hover:bg-slate-500/20'}`}>
                {Icon && <Icon size={16} color={pathname === route ? 'white' : 'black'} className={`relative top-0.5`} />}
                <span className={`ml-1 ${pathname === route ? 'text-white' : 'text-black'}`}>
                    {getNavlinkLabel(route)}
                </span>
            </div>
        </Link>
    )
}

function getNavlinkIcon(route: Route) {
    switch(route) {
        case routes.HOME: return AiFillHome
        case routes.GROUPS: return BsFillPeopleFill
        case routes.SETTINGS: return RiSettings4Fill
        default: return undefined
    }
}

function getNavlinkLabel(route: Route) {
    switch(route) {
        case routes.HOME: return 'Home'
        case routes.GROUPS: return 'Groups'
        case routes.SETTINGS: return 'Settings'
        default: return route
    }
}

export default Navlink
