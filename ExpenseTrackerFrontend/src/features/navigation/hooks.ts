import { useEffect } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import { useSelector } from '_/facade/redux/hooks'
import routes from '_/constants/routes'

function useRouteGuard() {
    const currentUser = useSelector(_ => _.auth.currentUser)
        , navigate = useNavigate()
        , location = useLocation()

    useEffect(
        () => {
            if (!currentUser
                    && ![routes.LOGIN, routes.REGISTER].some(_ => _ === location.pathname))
                navigate(routes.LOGIN, { state: { from: location } })

            if (currentUser
                    && [routes.EMPTY, routes.LOGIN, routes.REGISTER].some(_ => _ === location.pathname)) {

                const fromLocation = (location.state as any)?.from
                if (!fromLocation)
                    navigate(routes.HOME)
                else
                    navigate({...fromLocation})
            }
        },
        [currentUser, location]
    )
}

export {
    useRouteGuard,
}
