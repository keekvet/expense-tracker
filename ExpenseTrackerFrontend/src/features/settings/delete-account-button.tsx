import Button from '_/components/form/button'
import { useAction, useSelector } from '_/facade/redux/hooks'
import * as userEffects from '../user/effects'
import * as authEffects from '../auth/effects'
import * as toastEffects from '../toast/effects'

function DeleteAccountButton() {
    const currentUserId = useSelector(_ => _.auth.currentUser!.id)
        , deleteUser = useAction(userEffects.deleteUser)
        , toast = useAction(toastEffects.toast)
        , logout = useAction(authEffects.logout)

    function handleDeleteUser() {
        deleteUser({ id: currentUserId })
            .then(() => logout())
            .then(() => toast({ type: 'info', message: 'Your account has been removed' }))
    }

    return (
        <Button
            onClick={handleDeleteUser}
            className='btn--shadowed bg-pink-700 text-white'
        >
            Delete Account
        </Button>
    )
}

export default DeleteAccountButton
