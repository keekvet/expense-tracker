import { object, ref, SchemaOf, string } from 'yup'
import { ChangePassword } from '_/model/settings/types'

const changeNameSchema: SchemaOf<{ name: string }> = object({
    name: string().max(255).required()
})

const changePasswordSchema: SchemaOf<ChangePassword> = object({
    oldPassword: string().min(8).required(),
    newPassword: string().min(8).required(),
    newPasswordConfirmation: string().required().oneOf([ref('newPassword'), null], 'Must be equal to new password field'),
})

export {
    changeNameSchema,
    changePasswordSchema,
}