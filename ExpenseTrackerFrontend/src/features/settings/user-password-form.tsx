import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form'
import Button from '_/components/form/button'
import Input from '_/components/form/input'
import { useAction, useSelector } from '_/facade/redux/hooks'
import { changePasswordSchema } from './validate'
import * as userEffects from '../user/effects'
import * as toastEffects from '../toast/effects'
import { ChangePassword } from '_/model/settings/types'

function UserPasswordForm() {
    const { control, handleSubmit, reset } = useForm<ChangePassword>({
        defaultValues: { oldPassword: '', newPassword: '', newPasswordConfirmation: '' },
        resolver: yupResolver(changePasswordSchema),
    })
        , currentUser = useSelector(_ => _.auth.currentUser!)
        , updatePassword = useAction(userEffects.updatePassword)
        , toast = useAction(toastEffects.toast)

    function handleFormSubmit(formValue: ChangePassword) {
        updatePassword({
                userId: currentUser.id,
                oldPassword: formValue.oldPassword,
                newPassword: formValue.newPassword,
            })
            .then(_ => reset())
            .then(_ =>
                toast({ type: 'success', message: 'Password changed successfully' })
            )
    }

    return (
        <form onSubmit={handleSubmit(handleFormSubmit)} >
            <Input
                name='oldPassword'
                label='Old Password'
                control={control}
            />
            <Input
                name='newPassword'
                label='New Password'
                control={control}
            />
            <Input
                name='newPasswordConfirmation'
                label='Confirm New Password'
                control={control}
            />
            <Button className='btn--shadowed bg-yellow-300' type='submit'>Submit</Button>
        </form>
    )
}

export default UserPasswordForm
