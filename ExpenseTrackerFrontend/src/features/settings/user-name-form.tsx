import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form'
import Button from '_/components/form/button'
import Input from '_/components/form/input'
import { useAction, useSelector } from '_/facade/redux/hooks'
import { changeNameSchema } from './validate'
import * as userEffects from '../user/effects'
import * as toastEffects from '../toast/effects'

function UserNameForm() {
    const { control, handleSubmit } = useForm<{ name: string }>(
        { resolver: yupResolver(changeNameSchema) }
    )
        , currentUser = useSelector(_ => _.auth.currentUser!)
        , updateUsername = useAction(userEffects.updateUsername)
        , toast = useAction(toastEffects.toast)

    function handleFormSubmit({ name }: { name: string }) {
        updateUsername({ id: currentUser.id, name })
            .then(_ => {
                toast({ type: 'success', message: 'Name changed successfully' })
            })
    }

    return (
        <form onSubmit={handleSubmit(handleFormSubmit)} >
            <div className='flex items-end'>
                <Input
                    name='name'
                    label='Name'
                    defaultValue={currentUser.name}
                    containerClassName='mb-0'
                    className='mb-0'
                    control={control}
                />
                <div className='ml-4'>
                    <Button className='btn--shadowed bg-yellow-300' type='submit'>Submit</Button>
                </div>
            </div>
        </form>
    )
}

export default UserNameForm
