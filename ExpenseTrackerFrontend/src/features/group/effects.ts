import { getThunkCreator } from '_/utils/redux'
import api from '_/service/impl/group-service'

const thunk = getThunkCreator('group')

export const createGroup = thunk('createGroup', ({ name }: { name: string }) => api.create(name))

export const updateGroup = thunk('updateGroup', (data: { id: number, name: string }) => api.update(data))

export const getGroup = thunk('getGroup', ({ id }: { id: number }) => api.getById(id))

export const deleteGroup = thunk('deleteGroup', ({ id }: { id: number }) => api.delete(id))

export const getGroups = thunk('getGroups', ({ userId }: { userId: number }) => api.getAll(userId))

export const addUserToGroup = thunk(
    'addUserToGroup',
    ({ groupId, userId }: { groupId: number, userId: number }) =>
        api.addUserToGroup(groupId, userId)
)

export const deleteUserFromGroup = thunk(
    'deleteUserFromGroup',
    ({ groupId, userId }: { groupId: number, userId: number }) =>
        api.deleteUserFromGroup(groupId, userId)
)

export const getUsersByGroupId = thunk(
    'getUsersByGroupId',
    ({ groupId }: { groupId: number }) =>
        api.getUsersByGroupId(groupId)
)
