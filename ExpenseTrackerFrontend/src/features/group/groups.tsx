import { Link } from 'react-router-dom'
import { Group } from '_/model/group/types'
import routes from '_/constants/routes'

interface Props {
    groups: Group[]
}

function Groups(props: Props) {
    return (
        <div className='overflow-y-auto overflow-x-hidden'>
            {props.groups.map(_ => (
                <div className='border-2 border-black bg-emerald-300 rounded-2xl px-5 py-3 mb-4 flex justify-between' key={_.id}>
                    <div>
                        <span className='font-bold text-xl'>{_.name}</span>
                        <span className='block'>
                            {_.users.map(_ => (
                                <span className='ml-2' key={_.id}>
                                    {_.name}
                                </span>
                            ))}
                        </span>
                    </div>
                    <div className='self-center text-5xl font-bold'>
                        <Link to={routes.GROUPS + `/${_.id}`}>{'>'}</Link>
                    </div>
                </div>
            ))}
        </div>
    )
}

export default Groups
