import { Group } from '_/model/group/types'

interface Props {
    group: Group
}

function GroupInfo({ group }: Props) {
    return (
        <div>
            <div>
                <span className='font-bold text-xl'>{group.name}</span>
                <span>Info about you</span>
                <span className='block'>
                    {group.users.map(_ => (
                        <span className='ml-2' key={_.id}>
                            {_.name}
                        </span>
                    ))}
                </span>
            </div>
            <div>

            </div>

        </div>
    )
}

export default GroupInfo

function useExpenses(group: Group) {

}
