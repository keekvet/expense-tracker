import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form'
import Button from '_/components/form/button'
import Input from '_/components/form/input'
import { groupNameSchema } from './validate'

interface Props {
    onSubmit: (_: { name: string }) => void
}

function CreateGroupForm(props: Props) {
    const { control, handleSubmit } = useForm<{ name: string }>(
        { resolver: yupResolver(groupNameSchema) }
    )

    return (
        <form onSubmit={handleSubmit(props.onSubmit)} >
            <div className='flex items-end'>
                <Input
                    name='name'
                    label='Group name'
                    containerClassName='mb-0'
                    className='mb-0'
                    control={control}
                />
                <div className='ml-4'>
                    <Button className='btn--shadowed bg-yellow-300' type='submit'>Submit</Button>
                </div>
            </div>
        </form>
    )
}

export default CreateGroupForm
