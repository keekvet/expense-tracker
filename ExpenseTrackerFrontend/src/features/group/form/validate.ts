import { object, SchemaOf, string } from 'yup'

const groupNameSchema: SchemaOf<{ name: string }> = object({
    name: string().max(255).required()
})

export {
    groupNameSchema,
}
