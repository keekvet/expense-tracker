import { createAsyncThunk } from '@reduxjs/toolkit'
import { TOKEN_LIFESPAN } from '_/constants/auth'
import { Login, Registration } from '_/model/auth/types'
import api from '_/service/impl/auth-service'
import { debounce } from '_/utils/function'
import { authActions } from './reducer'

export const login = createAsyncThunk(
    'auth/login',
    async (data: Login, { dispatch }) => {
        const response = await api.login(data)
        dispatch(authActions.userLoggedIn(response))

        debounce(
            () => {
                setTimeout(
                    () => {
                        dispatch(authActions.userLoggedOut())
                    },
                    TOKEN_LIFESPAN
                )
            },
            TOKEN_LIFESPAN
        )()
    }
)

export const logout = createAsyncThunk(
    'auth/logout',
    (_, { dispatch }) =>
        dispatch(authActions.userLoggedOut())
)

export const register = createAsyncThunk(
    'auth/register',
    (data: Registration) => api.register(data)
)