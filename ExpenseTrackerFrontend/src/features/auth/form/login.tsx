import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { Login } from '../../../model/auth/types'
import { loginSchema } from './validate'
import { useAction } from '../../../facade/redux/hooks'
import * as authEffects from '../effects'
import Input from '_/components/form/input'
import Button from '_/components/form/button'

function LoginForm() {
    const login = useAction(authEffects.login)
        , { handleSubmit, control } = useForm<Login>({ resolver: yupResolver(loginSchema) })

    function handleFormSumbit(loginData: Login) {
        login(loginData)
    }

    return (
        <div className='px-6 py-8 h-full rounded-2xl bg-pink-200 border-2 border-black'>
            <form
                onSubmit={handleSubmit(handleFormSumbit)}
                className='h-full flex flex-col justify-between'
            >
                <div className='mb-2'>
                    <Input name='userName' label='Username' control={control}/>
                    <Input name='password' label='Password' type='password' control={control}/>
                </div>
                <div className='text-center'>
                    <Button className='btn--shadowed bg-yellow-300' type='submit'>Log in</Button>
                </div>
            </form>
        </div>
    )
}

export default LoginForm
