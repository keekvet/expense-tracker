import { Registration, Login } from '_/model/auth/types'
import { object, SchemaOf, string, ref } from 'yup'

const commomRules = {
    userName: string().max(255).required(),
    password: string().min(8).required(),
}

const loginSchema: SchemaOf<Login> = object({
    ...commomRules,
}) 

const registrationSchema: SchemaOf<Registration> = object({
    ...commomRules,
    passwordConfirmation: string().required().oneOf([ref('password'), null], 'Must be equal to password field'),
})

export {
    loginSchema,
    registrationSchema,
}