import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { Registration } from '_/model/auth/types'
import { registrationSchema } from './validate'
import { useAction } from '_/facade/redux/hooks'
import * as authEffects from '../effects'
import Input from '_/components/form/input'
import Button from '_/components/form/button'
import { useNavigate } from 'react-router-dom'
import routes from '_/constants/routes'
import { toast } from '_/features/toast/effects'


function RegisterForm() {
    const { control, handleSubmit } = useForm<Registration>(
        { resolver: yupResolver(registrationSchema) }
    )
        , registerUser = useAction(authEffects.register)
        , navigate = useNavigate()

    function handleFormSubmit(registerData: Registration) {
        registerUser(registerData)
            .then(() => navigate(routes.LOGIN))
            .then(() => toast({ type: 'success', message: 'Successfuly registrated' }))
    }

    return (
        <div className='px-6 py-8 h-full rounded-2xl bg-purple-200 border-2 border-black'>
            <form
                onSubmit={handleSubmit(handleFormSubmit)}
                className='h-full flex flex-col justify-between'
            >
                <div className='mb-2'>
                    <Input name='userName' label='Username' control={control}/>
                    <Input name='password' label='Password' type='password' control={control}/>
                    <Input name='passwordConfirmation' label='Confirm Password' type='password' control={control}/>
                </div>
                <div className='text-center'>
                    <Button className='btn--shadowed bg-yellow-300' type='submit'>Create</Button>
                </div>
            </form>
        </div>
    )
}

export default RegisterForm
