import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { LoginResponse } from '_/model/auth/types'
import { User } from '_/model/user/types'

interface AuthState {
    currentUser?: User
    token?: string
}

const initialState: AuthState = {}

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        userLoggedIn(_, action: PayloadAction<LoginResponse>) {
            return { currentUser: action.payload.user, token: action.payload.token }
        },
        userLoggedOut() {
            return initialState
        },
        userChangedName(_, action: PayloadAction<string>) {
            if (_.currentUser) _.currentUser.name = action.payload
        }
    }
})

export const authReducer = authSlice.reducer
export const authActions = authSlice.actions