import { useSelector } from '_/facade/redux/hooks'
import ToastCard from './toast-card'

function ToastsSpy() {
    const toasts = useSelector(_ => _.toasts)

    return (
        <div className='fixed z-50 -translate-x-2/4 left-2/4 top-4'>
            {toasts.toasts.map(_ => (
                <ToastCard toast={_} key={_.id} />
            ))}
        </div>
    )
}

export default ToastsSpy
