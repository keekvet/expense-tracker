import { createAsyncThunk } from '@reduxjs/toolkit';
import { Toast } from '_/model/toast/types';
import { toastsActions } from './reducer'
import { v4 as uuidv4 } from 'uuid'
import { TOAST_LIFESPAN } from '_/constants/auth';

export const toast = createAsyncThunk(
    'toast/add',
    (toast: Omit<Toast, 'id'>, { dispatch }) => {
        const newToast = { id: uuidv4(), ...toast }

        dispatch(toastsActions.toastAdded(newToast))
        setTimeout(
            () => {
                dispatch(toastsActions.toastRemoved(newToast))
            },
            TOAST_LIFESPAN
        )
    }
)
