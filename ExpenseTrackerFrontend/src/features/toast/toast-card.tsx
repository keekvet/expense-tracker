import { Toast } from '_/model/toast/types'
import { AiFillInfoCircle, AiFillWarning } from 'react-icons/ai'
import { ImCross } from 'react-icons/im'
import { GrStatusGood } from 'react-icons/gr'
import { useDispatch } from '_/facade/redux/hooks'
import { toastsActions } from './reducer'

interface Props {
    toast: Toast
}

function ToastCard({ toast }: Props) {
    const dispatch = useDispatch()

    function handleToastClose() {
        dispatch(toastsActions.toastRemoved(toast))
    }

    return (
        <div className={`h-28 w-80 py-3 px-4 mb-3 flex border-2 border-black rounded-2xl ${getCardClassName(toast.type)}`}>
            <div className='pr-3'>
                {getToastIcon(toast.type, '1.6em')}
            </div>
            <div>
                <p className='font-bold'>{getToastTypeName(toast.type)}</p>
                <p className='text-slate-900 leading-5 text-ellipsis' title={toast.message}>{toast.message}</p>
            </div>
            <div>
                <button onClick={handleToastClose}>
                    <ImCross size='0.6em' className='text-slate-900' />
                </button>
            </div>
        </div>
    )
}

export default ToastCard

function getToastTypeName(type: Toast['type']) {
    switch(type) {
        case 'success': return 'Success'
        case 'error': return 'Error'
        case 'warning': return 'Warning'
        case 'info': return 'Info'
    }
}

function getToastIcon(type: Toast['type'], size: string | number | undefined) {
    switch(type) {
        case 'success': return <GrStatusGood size={size} />
        case 'error': return <ImCross size={size} />
        case 'warning': return <AiFillWarning size={size} />
        case 'info': return <AiFillInfoCircle size={size} />
    }
}

function getCardClassName(type: Toast['type']) {
    switch(type) {
        case 'success': return 'bg-emerald-400'
        case 'error': return 'bg-red-300'
        case 'warning': return 'bg-yellow-300'
        case 'info': return 'bg-blue-300'
    }
}
