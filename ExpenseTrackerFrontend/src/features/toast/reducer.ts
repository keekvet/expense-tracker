import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Toast } from '_/model/toast/types'

interface ToastState {
    toasts: Toast[]
}

const initialState: ToastState = {
    toasts: []
}

export const toastSlice = createSlice({
    name: 'toast',
    initialState,
    reducers: {
        toastAdded(state, action: PayloadAction<Toast>) {
            state.toasts.push(action.payload)
        },
        toastRemoved(state, action: PayloadAction<Toast>) {
            state.toasts = state.toasts.filter(_ => _.id !== action.payload.id)
        },
    }
})

export const toastsReducer = toastSlice.reducer
export const toastsActions = toastSlice.actions
