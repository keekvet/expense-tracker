import DeleteAccountButton from '_/features/settings/delete-account-button'
import UserNameForm from '_/features/settings/user-name-form'
import UserPasswordForm from '_/features/settings/user-password-form'
import PageLayout from '../layout/page-layout'

function SettingsPage() {
    return (
        <PageLayout title='Settings'>
            <div className='w-1/3'>
                <div className='mb-4'>
                    <UserNameForm />
                </div>
                <hr className='mb-4 border-b-4 border-t-0 border-slate-300'/>
                <div className='mb-4'>
                    <UserPasswordForm />
                </div>
                <hr className='mb-4 border-b-4 border-t-0 border-slate-300'/>
                <div>
                    <DeleteAccountButton />
                </div>
            </div>
        </PageLayout>
    )
}

export default SettingsPage
