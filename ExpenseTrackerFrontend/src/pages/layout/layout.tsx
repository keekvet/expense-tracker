import Navbar from '../../features/navigation/navbar'

interface Props {
    children: React.ReactNode
}

function Layout({ children }: Props) {
    return (
        <>
            <Navbar />
            <div className='self-stretch flex-shrink-0 flex-grow ml-8 mirror-shadow'>
                <div className='flex flex-col min-h-full max-h-full py-3 px-5 border-2 border-black rounded-2xl bg-white'>
                    {children}
                </div>
            </div>
        </>
    )
}

export default Layout
