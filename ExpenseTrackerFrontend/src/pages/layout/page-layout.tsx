interface Props{
    title?: string
    children: React.ReactNode
}

function PageLayout({ title, children }: Props) {
    return (
        <>
            {title
                && <div className='mb-3'>
                    <span className='text-5xl font-bold capitalize underline decoration-8 decoration-purple-500'>
                        {title}
                    </span>
                </div>
            }
            { children }
        </>
    )
}

export default PageLayout
