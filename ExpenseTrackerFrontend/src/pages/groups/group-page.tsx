import { useCallback, useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { useAction } from '_/facade/redux/hooks'
import * as groupEffects from '_/features/group/effects'
import { Group } from '_/model/group/types'
import PageLayout from '../layout/page-layout'
import * as routes from '_/constants/routes'

function GroupPage() {
    const { id } = useParams()
        , convertedId = id !== undefined ? +id : 0
        , [group, _reload] = useGroup(convertedId)

    return (
        <PageLayout title={`Groups > ${group?.name || ''}`}>

        </PageLayout>
    )
}

export default GroupPage

function useGroup(id: number) {
    const [group, setGroup] = useState<Group>()
        , getGroup = useAction(groupEffects.getGroup)
        , [count, setCount] = useState(0)
        , reload = useCallback(() => setCount(_ => ++_), [setCount])
        , navigate = useNavigate()

    useEffect(
        () => {
            getGroup({ id })
                .then(setGroup)
                .catch(_ => {
                    navigate(routes.GROUPS)
                    return Promise.reject(_)
                })

        },
        [count, getGroup]
    )

    return [group, reload] as const
}
