import { useEffect, useState } from 'react'
import { useSearchParams } from 'react-router-dom'
import Button from '_/components/form/button'
import Modal from '_/components/modal'
import { ModalParam } from '_/constants/modal-params'
import mp from '_/constants/modal-params'
import { useAction, useSelector } from '_/facade/redux/hooks'
import * as groupEffects from '_/features/group/effects'
import Groups from '_/features/group/groups'
import { Group } from '_/model/group/types'
import PageLayout from '../layout/page-layout'
import CreateGroupForm from '_/features/group/form/create-group'

function GroupsPage() {
    const [groups, setGroups] = useGroups()
        , createGroup = useAction(groupEffects.createGroup)
        , [isOpen, openModal, closeModal] = useModalByParam(mp.CREATE_GROUP)

    function handleCreateGroupFormSubmit(formValue: { name: string }) {
        createGroup(formValue)
            .then(_ => setGroups(prevGroups => [...prevGroups, _]))
            .then(closeModal)
    }

    return (
        <PageLayout title='Groups'>
            <div className='flex justify-end mb-4'>
                <Button className='bg-red-300' onClick={openModal}>Add group</Button>
            </div>
            <Groups groups={groups} />
            {isOpen
                && <Modal onClose={closeModal}>
                    <CreateGroupForm onSubmit={handleCreateGroupFormSubmit} />
                </Modal>
            }
        </PageLayout>
    )
}

export default GroupsPage

function useGroups() {
    const [groups, setGroups] = useState<Group[]>([])
        , currentUserId = useSelector(_ => _.auth.currentUser!.id)
        , getGroups = useAction(groupEffects.getGroups)

    useEffect(
        () => {
            getGroups({ userId: currentUserId })
                .then(setGroups)
        },
        [getGroups]
    )

    return [groups, setGroups] as const
}

// function useModal() {
//     const [isOpen, setIsOpen] = useState(false)

//     function open() {
//         setIsOpen(false)
//     }

//     function close() {
//         setIsOpen(false)
//     }

//     return [isOpen, open, close] as const
// }

function useModalByParam(param: ModalParam) {
    const [searchParams, setSearchParams] = useSearchParams()
        , isOpen = searchParams.get(param.key) === param.value

    function open() {
        setSearchParams({ [param.key]: param.value })
    }

    function close() {
        setSearchParams({})
    }

    return [isOpen, open, close] as const
}
