
function ErrorPage() {
    return (
        <div className='h-full flex items-center justify-center'>
            <div className='text-center text-rose-800 font-bold text-3xl'>Page not found</div>
        </div>
    )
}

export default ErrorPage