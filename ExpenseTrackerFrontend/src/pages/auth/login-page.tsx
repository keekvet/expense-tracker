import Brand from '_/components/brand'
import LoginForm from '_/features/auth/form/login'
import { Link } from 'react-router-dom'
import routes from '_/constants/routes'

function LoginPage() {
    return (
        <div className='flex-grow flex justify-center items-center'>
            <div className='w-96'>
                <div className='mb-4 relative'>
                    <Brand />
                </div>
                <div className='mirror-shadow mb-4'>
                    <LoginForm />
                </div>
                <div className='text-center font-medium text-sm'>
                    <span>Dont have an account?&nbsp;</span>
                    <Link
                        to={routes.REGISTER}
                        className='underline decoration-2
                        hover:decoration-blue-600 bg-yellow-300'
                    >
                        Create now
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default LoginPage
