import Brand from '_/components/brand'
import { Link } from 'react-router-dom'
import routes from '_/constants/routes'
import RegisterForm from '_/features/auth/form/register'

function RegisterPage() {
    return (
        <div className='flex-grow flex justify-center items-center'>
            <div className='w-96'>
                <div className='mb-4 relative'>
                    <Brand />
                </div>
                <div className='mirror-shadow before:bg-purple-500 mb-4'>
                    <RegisterForm />
                </div>
                <div className='text-center font-medium text-sm'>
                    <span>Already have account?&nbsp;</span>
                    <Link
                        to={routes.LOGIN}
                        className='underline decoration-2
                        hover:decoration-blue-600 bg-yellow-300'
                    >
                        Log in
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default RegisterPage
