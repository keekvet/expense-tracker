import { useSelector } from '_/facade/redux/hooks'
import ToastCard from '_/features/toast/toast-card'
import { Toast } from '_/model/toast/types'
import PageLayout from '../layout/page-layout'

function HomePage() {
    const currentUser = useSelector(_ => _.auth.currentUser)!

    const toastWarning: Toast = {
        id: '000',
        type: 'warning',
        message: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. In cumque eius sequi obcaecati.'
    }

    const toastError: Toast = {
        id: '000',
        type: 'error',
        message: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. In cumque eius sequi obcaecati.'
    }

    const toastSuccess: Toast = {
        id: '000',
        type: 'success',
        message: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. In cumque eius sequi obcaecati.'
    }

    const toastInfo: Toast = {
        id: '000',
        type: 'info',
        message: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. In cumque eius sequi obcaecati.'
    }

    return (
        <PageLayout title='Home'>
                <p className='text-7xl font-bold capitalize mt-4'>
                    Welcome, <span className='underline decoration-8 decoration-yellow-300'>{currentUser.name}</span>👋🏻
                </p>
            {/* <ToastCard toast={toastWarning} ></ToastCard>
            <ToastCard toast={toastError} ></ToastCard>
            <ToastCard toast={toastSuccess} ></ToastCard>
            <ToastCard toast={toastInfo} ></ToastCard> */}
        </PageLayout>
    )
}

export default HomePage
