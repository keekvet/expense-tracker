import { AsyncThunkOptions, AsyncThunkPayloadCreator, createAsyncThunk } from '@reduxjs/toolkit'

function getThunkCreator(typePrefix?: string) {
    return function<Returned, ThunkArg = void>(type: string, payload: AsyncThunkPayloadCreator<Returned, ThunkArg, {}>, options?: AsyncThunkOptions<ThunkArg, {}>) {
        return createAsyncThunk(`${typePrefix ? `${typePrefix}/` : ''}${type}`, payload, options)
    }
}

export {
    getThunkCreator,
}
