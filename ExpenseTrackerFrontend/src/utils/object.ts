function omit<T extends object, F extends keyof T>(obj: T, ...props: F[]) {
    const result = { ...obj }
    props.forEach(_ => delete result[_])
    return result as Omit<T, F>
}

export {
    omit,
}