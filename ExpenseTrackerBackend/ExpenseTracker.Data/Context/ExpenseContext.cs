﻿using ExpenseTracker.Data.Extensions;
using ExpenseTracker.Data.Repositories.Base.Interfaces;
using ExpenseTracker.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ExpenseTracker.Data
{
    public class ExpenseContext : DbContext, IUnitOfWork
    {
        public ExpenseContext(DbContextOptions options) : base(options)
        {

        }
        
        public ExpenseContext()
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Expense> Expenses { get; set; }
        public DbSet<SubExpense> SubExpenses { get; set; }
        public DbSet<ExpenseComment> ExpenseComments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ConfigureEntities(); 
        }
    }   
}
