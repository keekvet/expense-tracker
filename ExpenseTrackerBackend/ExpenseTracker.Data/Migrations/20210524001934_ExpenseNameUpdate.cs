﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ExpenseTracker.Data.Migrations
{
    public partial class ExpenseNameUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SpendedAmount",
                table: "SubExpenses",
                newName: "SpentAmount");

            migrationBuilder.AddColumn<bool>(
                name: "IsAccepted",
                table: "Payments",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsAccepted",
                table: "Payments");

            migrationBuilder.RenameColumn(
                name: "SpentAmount",
                table: "SubExpenses",
                newName: "SpendedAmount");
        }
    }
}
