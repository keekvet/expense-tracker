﻿using ExpenseTracker.Data.Configurations;
using Microsoft.EntityFrameworkCore;

namespace ExpenseTracker.Data.Extensions
{
    public static class DbConfigurationExtension
    {
        public static ModelBuilder ConfigureEntities(this ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new GroupConfiguration());
            modelBuilder.ApplyConfiguration(new PaymentConfiguration());
            modelBuilder.ApplyConfiguration(new ExpenseConfiguration());
            modelBuilder.ApplyConfiguration(new SubExpenseConfiguration());
            modelBuilder.ApplyConfiguration(new ExpenseCommentConfiguration());

            return modelBuilder;
        }
    }
}