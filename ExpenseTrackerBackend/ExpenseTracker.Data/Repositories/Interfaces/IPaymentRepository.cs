﻿using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Data.Repositories.Interfaces
{
    public interface IPaymentRepository : IRepository<Payment, int>
    {
        
    }
}
