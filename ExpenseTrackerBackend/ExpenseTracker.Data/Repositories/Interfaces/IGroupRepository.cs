﻿using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Data.Repositories.Interfaces
{
    public interface IGroupRepository : IRepository<Group, int>
    {
        
    }
}
