﻿using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Data.Repositories.Interfaces
{
    public interface IExpenseRepository : IRepository<Expense, int>
    {
        
    }
}
