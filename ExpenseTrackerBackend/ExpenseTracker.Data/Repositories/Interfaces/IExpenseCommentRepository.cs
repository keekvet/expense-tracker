﻿using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Data.Repositories.Interfaces
{
    public interface IExpenseCommentRepository : IRepository<ExpenseComment, int>
    {
        
    }
}
