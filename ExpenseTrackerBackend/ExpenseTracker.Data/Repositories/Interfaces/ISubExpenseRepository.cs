﻿using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Data.Repositories.Interfaces
{
    public interface ISubExpenseRepository : IRepository<SubExpense, int>
    {
        
    }
}
