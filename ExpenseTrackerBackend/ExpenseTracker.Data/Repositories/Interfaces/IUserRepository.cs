﻿using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Data.Repositories.Interfaces
{
    public interface IUserRepository : IRepository<User, int>
    {
    }
}
