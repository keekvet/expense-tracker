﻿using ExpenseTracker.Data.Repositories.Base.Implementations;
using ExpenseTracker.Data.Repositories.Interfaces;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Data.Repositories.Implementations
{
    public class GroupRepository : BaseEfRepository<Group, int, ExpenseContext>, IGroupRepository
    {
        public GroupRepository(ExpenseContext context) : base(context)
        {
        }
    }
}
 