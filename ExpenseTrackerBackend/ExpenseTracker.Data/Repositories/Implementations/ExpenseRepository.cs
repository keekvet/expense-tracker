﻿using ExpenseTracker.Data.Repositories.Base.Implementations;
using ExpenseTracker.Data.Repositories.Interfaces;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Data.Repositories.Implementations
{
    public class ExpenseRepository : BaseEfRepository<Expense, int, ExpenseContext>, IExpenseRepository
    {
        public ExpenseRepository(ExpenseContext context) : base(context)
        {
        }
    }
}
