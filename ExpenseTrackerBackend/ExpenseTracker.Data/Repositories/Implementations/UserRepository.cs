﻿using ExpenseTracker.Data.Repositories.Base.Implementations;
using ExpenseTracker.Data.Repositories.Interfaces;
using ExpenseTracker.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ExpenseTracker.Data.Repositories.Implementations
{
    public class UserRepository : BaseEfRepository<User, int, ExpenseContext>, IUserRepository
    {
        public UserRepository(ExpenseContext context) : base(context)
        {
        }

        public override Task DeleteAsync(User entity)
        {
            context.Payments.RemoveRange(entity.IncomePayments);
            return base.DeleteAsync(entity);
        }

        public override Task DeleteRangeAsync(IEnumerable<User> entities)
        {
            context.Payments.RemoveRange(entities.SelectMany(e => e.IncomePayments));
            return base.DeleteRangeAsync(entities);
        }
    }
}
