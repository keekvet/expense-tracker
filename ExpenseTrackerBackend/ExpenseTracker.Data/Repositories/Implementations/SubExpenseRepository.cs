﻿using ExpenseTracker.Data.Repositories.Base.Implementations;
using ExpenseTracker.Data.Repositories.Interfaces;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Data.Repositories.Implementations
{
    public class SubExpenseRepository : BaseEfRepository<SubExpense, int, ExpenseContext>, ISubExpenseRepository
    {
        public SubExpenseRepository(ExpenseContext context) : base(context)
        {
        }
    }
}
