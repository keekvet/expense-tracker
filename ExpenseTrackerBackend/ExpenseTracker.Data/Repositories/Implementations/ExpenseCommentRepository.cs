﻿using ExpenseTracker.Data.Repositories.Base.Implementations;
using ExpenseTracker.Data.Repositories.Interfaces;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Data.Repositories.Implementations
{
    public class ExpenseCommentRepository : 
        BaseEfRepository<ExpenseComment, int, ExpenseContext>, 
        IExpenseCommentRepository
    {
        public ExpenseCommentRepository(ExpenseContext context) : base(context)
        {
        }
    }
}
 