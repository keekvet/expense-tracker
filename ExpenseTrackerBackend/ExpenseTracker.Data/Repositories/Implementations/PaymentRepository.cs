﻿using ExpenseTracker.Data.Repositories.Base.Implementations;
using ExpenseTracker.Data.Repositories.Interfaces;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Data.Repositories.Implementations
{
    public class PaymentRepository : BaseEfRepository<Payment, int, ExpenseContext>, IPaymentRepository
    {
        public PaymentRepository(ExpenseContext context) : base(context)
        {
        }
    }
}
