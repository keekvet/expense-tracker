﻿using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using ExpenseTracker.Data.Repositories.Base.Interfaces;
using ExpenseTracker.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ExpenseTracker.Data.Repositories.Base.Implementations
{
    public class BaseEfRepository<TEntity, TKey, TContext> : IRepository<TEntity, TKey>
        where TContext : DbContext, IUnitOfWork
        where TEntity : class, Domain.Models.Interfaces.IEntity<TKey>
    {
        protected readonly TContext context;
        protected readonly DbSet<TEntity> dbEntities;
        public BaseEfRepository(TContext context)
        {
            this.context = context;
            dbEntities = context.Set<TEntity>();
        }

        public IUnitOfWork UnitOfWork => context;

        public virtual Task<TEntity> GetByIdAsync(TKey id)
        {
            return dbEntities.FirstOrDefaultAsync(e => e.Id.Equals(id));
        }
        
        public virtual Task<TEntity> AddAsync(TEntity entity)
        {
            return Task.FromResult(dbEntities.Add(entity).Entity);
        }
        
        public virtual Task AddRangeAsync(IEnumerable<TEntity> entities)
        {
            dbEntities.AddRange(entities);
            return Task.CompletedTask;
        }

        public virtual Task DeleteAsync(TEntity entity)
        {
            dbEntities.Remove(entity);
            return Task.CompletedTask;
        }

        public virtual Task DeleteRangeAsync(IEnumerable<TEntity> entities)
        {
            dbEntities.RemoveRange(entities);
            return Task.CompletedTask;
        }

        public virtual Task UpdateAsync(TEntity entity)
        {
            dbEntities.Update(entity);
            return Task.CompletedTask;
        }

        public virtual Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            var changedRaws = UnitOfWork.SaveChangesAsync(cancellationToken);
            return changedRaws;
        }

        public Task<TEntity> SingleAsync(params ISpecification<TEntity>[] specifications)
        {
            return ApplySpecification(specifications).FirstOrDefaultAsync();
        }

        public Task<List<TEntity>> ListAsync(params ISpecification<TEntity>[] specifications)
        {
            return ApplySpecification(specifications).ToListAsync();
        }

        private IQueryable<TEntity> ApplySpecification(params ISpecification<TEntity>[] specifications)
        {
            var evaluator = new SpecificationEvaluator();
            var query = dbEntities.AsQueryable<TEntity>();
            
            foreach (var spec in specifications)
                query = evaluator.GetQuery(query, spec);

            return query;
        }
    }
}
