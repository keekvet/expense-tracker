﻿using System.Threading;
using System.Threading.Tasks;

namespace ExpenseTracker.Data.Repositories.Base.Interfaces
{
    public interface IUnitOfWork
    {
        public Task<int> SaveChangesAsync(CancellationToken token = default);
    }
}
