﻿using Ardalis.Specification;
using ExpenseTracker.Data.Repositories.Base.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ExpenseTracker.Data.Repositories.Interfaces
{
    public interface IRepository<TEntity, TKey> 
        where TEntity : class, Domain.Models.Interfaces.IEntity<TKey>
    {
        IUnitOfWork UnitOfWork{ get; }
        Task<TEntity> GetByIdAsync(TKey id);
        Task<TEntity> AddAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task DeleteAsync(TEntity entity);
        Task AddRangeAsync(IEnumerable<TEntity> entities);
        Task DeleteRangeAsync(IEnumerable<TEntity> entities);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        Task<List<TEntity>> ListAsync(params ISpecification<TEntity>[] specifications);
        Task<TEntity> SingleAsync(params ISpecification<TEntity>[] specifications);
    }
}
