﻿using ExpenseTracker.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ExpenseTracker.Data.Configurations
{
    public class ExpenseCommentConfiguration : IEntityTypeConfiguration<ExpenseComment>
    {
        public void Configure(EntityTypeBuilder<ExpenseComment> builder)
        {
            builder.Property(e => e.Message).HasMaxLength(500).IsRequired();

            builder.Property(e => e.Date).IsRequired();

            builder
                .HasOne(e => e.User)
                .WithMany(u => u.Comments)
                .HasForeignKey(e => e.UserId)
                .IsRequired();

            builder
                .HasOne(e => e.Expense)
                .WithMany(e => e.Comments)
                .HasForeignKey(e => e.ExpenseId)
                .IsRequired();
        }
    }
}
