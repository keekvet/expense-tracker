﻿using ExpenseTracker.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ExpenseTracker.Data.Configurations
{
    public class PaymentConfiguration : IEntityTypeConfiguration<Payment>
    {
        public void Configure(EntityTypeBuilder<Payment> builder)
        {
            builder.Property(p => p.Amount).HasPrecision(38, 2).IsRequired();
            builder.Property(p => p.Note).IsRequired().HasMaxLength(500);
            builder.Property(p => p.Date).IsRequired();

            builder
                .HasOne(p => p.Sender)
                .WithMany(u => u.OutputPayments)
                .HasForeignKey(p => p.SenderId)
                .IsRequired();

            builder
                .HasOne(p => p.Receiver)
                .WithMany(u => u.IncomePayments)
                .HasForeignKey(p => p.ReceiverId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(p => p.Expense)
                .WithMany(e => e.Payments)
                .HasForeignKey(p => p.ExpenseId)
                .IsRequired();
        }
    }
}
