﻿using ExpenseTracker.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ExpenseTracker.Data.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(u => u.Name)
                .HasMaxLength(50)
                .IsRequired();

            builder
                .Property(u => u.PasswordHash)
                .HasMaxLength(128);

            builder
                .Property(u => u.Salt)
                .HasMaxLength(20);

            builder
                .HasMany(u => u.Groups)
                .WithMany(g => g.Users);
        }
    }
}
