﻿using ExpenseTracker.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ExpenseTracker.Data.Configurations
{
    public class SubExpenseConfiguration : IEntityTypeConfiguration<SubExpense>
    {
        public void Configure(EntityTypeBuilder<SubExpense> builder)
        {
            builder.Property(s => s.SplitValue).HasPrecision(38, 2);
            builder.Property(s => s.SpentAmount).HasPrecision(38, 2);

            builder
                .HasOne(s => s.Expense)
                .WithMany(e => e.SubExpenses)
                .HasForeignKey(s => s.ExpenseId)
                .IsRequired();

            builder
                .HasOne(s => s.User)
                .WithMany(u => u.SubExpenses)
                .HasForeignKey(s => s.UserId)
                .IsRequired();
        }
    }
}
