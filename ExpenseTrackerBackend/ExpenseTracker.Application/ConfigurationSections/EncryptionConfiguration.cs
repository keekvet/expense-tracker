﻿namespace ExpenseTracker.Application.ConfigurationSections
{
    public class EncryptionConfiguration
    {
        public int Iterations { get; set; }
        public int PasswordHashLength { get; set; }
        public int SaltLength { get; set; }
        public string Pepper { get; set; }
    }
}
