﻿namespace ExpenseTracker.Application.ConfigurationSections
{
    public class JwtConfiguration
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string SigningKey { get; set; }
        public int LifetimeDays { get; set; }
    }
}
