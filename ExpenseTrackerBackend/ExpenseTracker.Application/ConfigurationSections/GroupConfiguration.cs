﻿namespace ExpenseTracker.Application.ConfigurationSections
{
    public class GroupConfiguration
    {
        public int MaxUsersPerGroup { get; set; }
    }
}
