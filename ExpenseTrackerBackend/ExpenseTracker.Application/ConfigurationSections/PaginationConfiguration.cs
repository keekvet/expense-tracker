﻿namespace ExpenseTracker.Application.ConfigurationSections
{
    public class PaginationConfiguration
    {
        public int MaxItemsPerPage { get; set; }
    }
}
