﻿using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Application.Services.Interfaces;
using ExpenseTracker.Application.Specifications.ExpenseComments;
using ExpenseTracker.Application.Specifications.Shared;
using ExpenseTracker.Data.Repositories.Interfaces;
using ExpenseTracker.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExpenseTracker.Application.Services.Implementations
{
    public class ExpenseCommentService : IExpenseCommentService
    {
        private readonly IExpenseCommentRepository _expenseCommentRepository;

        public ExpenseCommentService(IExpenseCommentRepository expenseCommentRepository)
        {
            _expenseCommentRepository = expenseCommentRepository;
        }

        public async Task<ExpenseComment> AddExpenseCommentAsync(ExpenseComment expenseComment)
        {
            expenseComment.Date = DateTime.UtcNow;
            
            expenseComment = await _expenseCommentRepository.AddAsync(expenseComment);
            await _expenseCommentRepository.SaveChangesAsync();

            return expenseComment;
        }

        public async Task DeleteExpenseCommentAsync(int id)
        {
            var expsenseComment = await _expenseCommentRepository.GetByIdAsync(id);

            await _expenseCommentRepository.DeleteAsync(expsenseComment);
            await _expenseCommentRepository.SaveChangesAsync();
        }

        public async Task<bool> CheckCommentBelongsToUserAsync(int expenseCommentId, int userId)
        {
            var comment = await _expenseCommentRepository.SingleAsync(
                new ExpenseCommentByIdAndUserIdSpec(expenseCommentId, userId));

            return comment is not null;
        }

        public async Task<IEnumerable<ExpenseComment>> GetAllByExpenseIdAsync(
            int expenseId, 
            PaginationModel paginationModel)
        {
            return await _expenseCommentRepository.ListAsync(
                new ExpenseCommentsByExpenseIdSpec(expenseId),
                new PaginationSpec<ExpenseComment>(paginationModel));
        }
    }
}
