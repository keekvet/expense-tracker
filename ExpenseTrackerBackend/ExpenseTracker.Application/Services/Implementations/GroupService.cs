﻿using ExpenseTracker.Application.Services.Interfaces;
using ExpenseTracker.Data.Repositories.Interfaces;
using ExpenseTracker.Domain.Models;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Extensions.Options;
using ExpenseTracker.Application.ConfigurationSections;
using System;
using System.Collections.Generic;
using ExpenseTracker.Application.Specifications.Groups;
using ExpenseTracker.Application.Specifications.Shared;
using Ardalis.Specification;
using ExpenseTracker.Application.Exceptions;
using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Application.Specifications.Expenses;
using ExpenseTracker.Application.Extensions;

namespace ExpenseTracker.Application.Services.Implementations
{
    public class GroupService : IGroupService
    {
        private readonly IUserRepository _userRepository;
        private readonly IGroupRepository _groupRepository;
        private readonly IExpenseRepository _expenseRepository;

        private readonly GroupConfiguration _groupConfig;

        public GroupService(
            IUserRepository userRepository, 
            IGroupRepository groupRepository,
            IExpenseRepository expenseRepository,
            IOptions<GroupConfiguration> groupConfig)
        {
            _userRepository = userRepository;
            _groupRepository = groupRepository;
            _expenseRepository = expenseRepository;
            _groupConfig = groupConfig.Value;
        }

        public async Task<Group> AddGroupAsync(Group group, int userId)
        {
            group = await _groupRepository.AddAsync(group);
            group.Users.Add(await _userRepository.GetByIdAsync(userId));
            await _groupRepository.SaveChangesAsync();

            return group;
        }

        public async Task<Group> GetGroupAsync(int id)
        {
            var group = await _groupRepository.SingleAsync(new GroupByIdIncludeUsersSpec(id));

            return group;
        }

        public async Task<IEnumerable<Group>> GetAllByUserAsync(int userId, PaginationModel paginationModel)
        {
            var userGroups = await _groupRepository.ListAsync(
                    new GroupsByUserIdSpec(userId),
                    new PaginationSpec<Group>(paginationModel));

            return userGroups;
        }

        public async Task UpdateGroupAsync(Group group)
        {
            await _groupRepository.UpdateAsync(group);
            await _groupRepository.SaveChangesAsync();
        }

        public async Task<Group> GetGroupByUserAsync(int groupId, int userId)
        {
            return await _groupRepository.SingleAsync(
                    new GroupByIdIncludeUsersSpec(groupId),
                    new GroupsByUserIdSpec(userId));
        }


        public async Task DeleteGroupAsync(int groupId)
        {
            var group = await _groupRepository.GetByIdAsync(groupId);

            if(group is null)
            {
                throw new NotFoundException("group");
            }
            if (group.Users.Count > 1)
            {
                throw new HttpException("can't delete group when there more than one user");
            }

            await _groupRepository.DeleteAsync(group);
            await _groupRepository.SaveChangesAsync();
        }

        public async Task AddUserToGroupAsync(int userId, int groupId)
        {
            var user = await _userRepository.GetByIdAsync(userId);
            var group = await _groupRepository.SingleAsync(new GroupByIdIncludeUsersSpec(groupId));

            if (user is null)
            {
                throw new NotFoundException("user");
            }
            if (group is null)
            {
                throw new NotFoundException("group");
            }
            if (group.Users.Count >= _groupConfig.MaxUsersPerGroup)
            {
                throw new HttpException($"can't add more than {_groupConfig.MaxUsersPerGroup} users");
            }
            if (group.Users.Any(u => u.Id == user.Id))
            {
                throw new HttpException("user already in group");
            }

            group.Users.Clear();
            group.Users.Add(user);

            await _groupRepository.UpdateAsync(group);
            await _groupRepository.SaveChangesAsync();
        }

        public async Task RemoveUserFromGroupAsync(int userId, int groupId)
        {
            var group = await _groupRepository.SingleAsync(
                new GroupByIdIncludeUsersSpec(groupId));

            if (group is null)
            {
                throw new NotFoundException("group");
            }

            var userExpenses = await _expenseRepository.ListAsync(
                new ExpensesByUserIdSpec(userId),
                new ExpensesIncludeSubExpensesUsersPaymentsSpec());

            bool isInDept = userExpenses.Select(u => u.CalculateUserMoneyDifference(userId)).Any(d => d > 0);

            if (isInDept)
            {
                throw new HttpException("user can't be deleted while debt exist");
            }

            var user = group.Users.FirstOrDefault(u => u.Id == userId);
            group.Users.Remove(user);

            await _groupRepository.UpdateAsync(group);
            await _groupRepository.SaveChangesAsync();
        }
        public async Task<bool> CheckGroupContainsUserAsync(int groupId, int userId)
        {
            return await GetGroupByUserAsync(groupId, userId) is not null;
        }
    }
}
