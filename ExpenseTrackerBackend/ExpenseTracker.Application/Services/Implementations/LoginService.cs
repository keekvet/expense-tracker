﻿using ExpenseTracker.Application.Exceptions;
using ExpenseTracker.Application.Models;
using ExpenseTracker.Application.Services.Interfaces;
using ExpenseTracker.Application.Specifications.Users;
using ExpenseTracker.Data.Repositories.Interfaces;
using System.Threading.Tasks;

namespace ExpenseTracker.Application.Services
{
    public class LoginService : ILoginService
    {
        private readonly IJwtService _jwtService;
        private readonly IUserRepository _userRepository;
        private readonly IEncryptionService _encryptionService;

        public LoginService(
            IJwtService jwtService,
            IUserRepository userRepository, 
            IEncryptionService encryptionService)
        {
            _jwtService = jwtService;
            _userRepository = userRepository;
            _encryptionService = encryptionService;
        }

        public async Task<UserTokenModel> LoginAsync(string userName, string password)
        {
            var user = await _userRepository.SingleAsync(new UserByNameSpec(userName));
            
            if (user is null)
            {
                throw new NotFoundException("user");
            }

            if (!_encryptionService.PasswordEqualsHash(password, user.PasswordHash, user.Salt))
            {
                throw new HttpException("wrong password");
            }

            return new UserTokenModel() {
                Token = _jwtService.CreateToken(user),
                User = user
            };
        }
    }
}
