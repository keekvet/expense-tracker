﻿using AutoMapper;
using ExpenseTracker.Application.Exceptions;
using ExpenseTracker.Application.Extensions;
using ExpenseTracker.Application.Models;
using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Application.Services.Interfaces;
using ExpenseTracker.Application.Specifications.Expenses;
using ExpenseTracker.Application.Specifications.Shared;
using ExpenseTracker.Application.Specifications.Users;
using ExpenseTracker.Data.Repositories.Interfaces;
using ExpenseTracker.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseTracker.Application.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;
        private readonly IExpenseRepository _expenseRepository;
        private readonly IEncryptionService _encryptionService;

        public UserService(
            IMapper mapper,
            IUserRepository userRepository, 
            IExpenseRepository expenseRepository,
            IEncryptionService encryptionService)
        {
            _mapper = mapper;
            _userRepository = userRepository;
            _expenseRepository = expenseRepository;
            _encryptionService = encryptionService;
        }

        public async Task AddUserAsync(RegistrationModel registrationModel)
        {
            var password = _encryptionService.Encrypt(Encoding.UTF8.GetBytes(registrationModel.Password));

            if (await _userRepository.SingleAsync(new UserByNameSpec(registrationModel.UserName)) is not null)
            {
                throw new HttpException("user with this name exist");
            }

            await _userRepository.AddAsync(
                new User() 
                { 
                    Name = registrationModel.UserName, 
                    Salt = password.Salt,
                    PasswordHash = password.HashPassword
                });
            await _userRepository.SaveChangesAsync();
        }

        public async Task<User> GetUserAsync(int id)
        {
            return await _userRepository.GetByIdAsync(id);
        }

        public async Task<IEnumerable<User>> GetAllByGroupAsync(int groupId, PaginationModel paginationModel)
        {
            var users = await _userRepository
                .ListAsync(
                    new UsersByGroupIdSpec(groupId),
                    new PaginationSpec<User>(paginationModel));

            return users;
        }

        public async Task UpdateUserAsync(User user)
        {
            if (await _userRepository.SingleAsync(new UserByNameSpec(user.Name)) is not null)
            {
                throw new HttpException("user with this name exist");
            }

            var userToUpdate = await _userRepository.GetByIdAsync(user.Id);

            user = _mapper.Map(user, userToUpdate);

            await _userRepository.UpdateAsync(user);
            await _userRepository.SaveChangesAsync();
        }

        public async Task UpdatePasswordAsync(UserPasswordUpdateModel userPasswordUpdateModel)
        {
            var user = await _userRepository.GetByIdAsync(userPasswordUpdateModel.UserId);

            if(!_encryptionService.PasswordEqualsHash(userPasswordUpdateModel.OldPassword, user.PasswordHash, user.Salt))
            {
                throw new HttpException("wrong password");
            }

            var newPassword = 
                _encryptionService.Encrypt(Encoding.UTF8.GetBytes(userPasswordUpdateModel.NewPassword));

            user.PasswordHash = newPassword.HashPassword;
            user.Salt = newPassword.Salt;

            await _userRepository.UpdateAsync(user);
            await _userRepository.SaveChangesAsync();
        }



        public async Task DeleteUserAsync(int userId)
        {
            var userExpenses = await _expenseRepository.ListAsync(
                new ExpensesByUserIdSpec(userId),
                new ExpensesIncludeSubExpensesUsersPaymentsSpec());

            bool isInDept = userExpenses.Select(u => u.CalculateUserMoneyDifference(userId)).Any(d => d > 0);

            if(isInDept)
            {
                throw new HttpException("user can't be deleted while debt exist");
            }

            var user = userExpenses.FirstOrDefault()?.SubExpenses?.FirstOrDefault()?.User;

            await _userRepository.DeleteAsync(user);
            await _userRepository.SaveChangesAsync();
        }
    }
}
