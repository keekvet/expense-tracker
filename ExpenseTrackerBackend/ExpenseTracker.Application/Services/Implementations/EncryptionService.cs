﻿using ExpenseTracker.Application.ConfigurationSections;
using ExpenseTracker.Application.Models;
using ExpenseTracker.Application.Services.Interfaces;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ExpenseTracker.Application.Services.Implementations
{
    public class EncryptionService : IEncryptionService
    {
        private readonly EncryptionConfiguration _encryptionConfig;

        public EncryptionService(IOptions<EncryptionConfiguration> encryptionConfig)
        {
            _encryptionConfig = encryptionConfig.Value;
        }        

        private byte[] Encrypt(byte[] data, byte[] salt)
        {
            var pepper = Encoding.UTF8.GetBytes(_encryptionConfig.Pepper);

            var pepperedPassword = data.Zip(pepper, (d, p) => (byte)(d + p)).ToArray();

            return new Rfc2898DeriveBytes(data, salt, _encryptionConfig.Iterations)
                .GetBytes(_encryptionConfig.PasswordHashLength);
        }

        public HashedPasswordModel Encrypt(byte[] data)
        {
            var salt = new byte[_encryptionConfig.SaltLength];
            new RNGCryptoServiceProvider().GetBytes(salt);

            return new HashedPasswordModel() 
            { 
                HashPassword = Encrypt(data, salt), 
                Salt = salt 
            };
        } 

        public bool PasswordEqualsHash(string password, byte[] hashToCompare, byte[] salt)
        {
            var hash = Encrypt(Encoding.UTF8.GetBytes(password), salt);
            
            return Enumerable.SequenceEqual(hash, hashToCompare);
        }
    }
}
