﻿using AutoMapper;
using ExpenseTracker.Application.Exceptions;
using ExpenseTracker.Application.Extensions;
using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Application.Services.Interfaces;
using ExpenseTracker.Application.Specifications.Expenses;
using ExpenseTracker.Application.Specifications.Shared;
using ExpenseTracker.Application.Specifications.SubExpenses;
using ExpenseTracker.Data.Repositories.Interfaces;
using ExpenseTracker.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExpenseTracker.Application.Services.Implementations
{
    public class SubExpenseService : ISubExpenseService
    {
        private readonly IMapper _mapper;
        private readonly IExpenseService _expenseService;
        private readonly IExpenseRepository _expenseRepository;
        private readonly ISubExpenseRepository _subExpenseRepository;

        public SubExpenseService(
            IMapper mapper,
            IExpenseService expenseService,
            IExpenseRepository expenseRepository,
            ISubExpenseRepository subExpenseRepository)
        {
            _mapper = mapper;
            _expenseService = expenseService;
            _expenseRepository = expenseRepository;
            _subExpenseRepository = subExpenseRepository;
        }

        public async Task<SubExpense> AddSubExpenseAsync(SubExpense subExpense)
        {
            var expense = await _expenseRepository.SingleAsync(
                new ExpenseByIdIncludeSubExpensesSpec(subExpense.ExpenseId));

            if(expense is null)
            {
                throw new NotFoundException("expense");
            }

            if (expense.ContainsUser())
            {
                throw new HttpException("user already in expense");
            }

            expense.SubExpenses.Add(subExpense);

            if (!expense.SubExpenses.CheckSubExpensesCorrect(expense.SplitType))
            {
                throw new HttpException("wrong expense values");
            }

            subExpense = await _subExpenseRepository.AddAsync(subExpense);
            await _subExpenseRepository.SaveChangesAsync();

            return subExpense;
        }

        public async Task<SubExpense> GetSubExpenseAsync(int id)
        {
            return await _subExpenseRepository.GetByIdAsync(id);
        }

        public async Task<IEnumerable<SubExpense>> GetAllByExpenseAsync(int expenseId, PaginationModel paginationModel)
        {
            return await _subExpenseRepository.ListAsync(
                new SubExpensesByExpenseIdSpec(expenseId),
                new PaginationSpec<SubExpense>(paginationModel));
        }

        public async Task<IEnumerable<SubExpense>> GetAllByUserAsync(int userId, PaginationModel paginationModel)
        {
            return await _subExpenseRepository.ListAsync(
                new SubExpensesByUserIdSpec(userId),
                new PaginationSpec<SubExpense>(paginationModel));
        }

        public async Task UpdateSubExpenseAsync(SubExpense subExpense)
        {
            var subExpenses = await _subExpenseRepository.ListAsync(
                new SubExpensesByExpenseIdSpec(subExpense.ExpenseId));

            subExpenses = subExpenses.Where(s => s.Id != subExpense.Id).ToList();

            subExpenses.Add(subExpense);

            if (!subExpenses.CheckSubExpensesCorrect(subExpense.Expense.SplitType))
            {
                throw new HttpException("can't update if it cause wrong values in expense");
            }

            await _subExpenseRepository.UpdateAsync(subExpense);
            await _subExpenseRepository.SaveChangesAsync();
        }

        public async Task DeleteSubExpenseAsync(int id)
        {
            var subExpnense = await _subExpenseRepository.SingleAsync(
                new SubExpenseByIdIncludeExpenseSpec(id));

            if (subExpnense is null)
            {
                throw new NotFoundException("subExpense");
            }

            var subExpenses = await _subExpenseRepository.ListAsync(
                new SubExpensesByExpenseIdSpec(subExpnense.ExpenseId));

            subExpenses = subExpenses.Where(s => s.Id != id).ToList();

            if (!subExpenses.CheckSubExpensesCorrect(subExpnense.Expense.SplitType))
            {
                throw new HttpException("can't delete if it cause wrong values in expense");
            }

            await _subExpenseRepository.DeleteAsync(subExpnense);
            await _subExpenseRepository.SaveChangesAsync();
        }

        public async Task<bool> CheckSubExpenseInSameGroupWithUserAsync(int subExpenseId, int userId)
        {
            return await _subExpenseRepository.SingleAsync(
                new SubExpensesInSameGroupWithUserSpec(userId),
                new SubExpenseByIdSpec(subExpenseId)) is not null;
        }

        public async Task ReplaceInExpenseAsync(int expenseId, IEnumerable<SubExpense> subExpenses)
        {
            var expense = await _expenseRepository.SingleAsync(
                new ExpenseByIdIncludeSubExpensesSpec(expenseId));

            foreach (var subExpense in subExpenses)
            {
                if (!await _expenseService.CheckExpenseInSameGroupWithUserAsync(expenseId, subExpense.UserId))
                {
                    throw new HttpException("user not in group");
                }
            }

            subExpenses = subExpenses
                .Select(s => {
                    s.ExpenseId = expenseId;
                    return s;
                });

            if (!subExpenses.CheckSubExpensesCorrect(expense.SplitType))
            {
                throw new HttpException("can't replace if it cause wrong values in expense");
            }

            await _subExpenseRepository.DeleteRangeAsync(expense.SubExpenses);

            expense.SubExpenses.Clear();

            foreach (var subExpenseToAdd in subExpenses)
            {
                expense.SubExpenses.Add(subExpenseToAdd);
            }

            await _expenseRepository.UpdateAsync(expense);
            await _expenseRepository.SaveChangesAsync();
        }

    }
}
