﻿using AutoMapper;
using ExpenseTracker.Application.Exceptions;
using ExpenseTracker.Application.Extensions;
using ExpenseTracker.Application.Models;
using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Application.Services.Interfaces;
using ExpenseTracker.Application.Specifications.Expenses;
using ExpenseTracker.Application.Specifications.Shared;
using ExpenseTracker.Data.Repositories.Interfaces;
using ExpenseTracker.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExpenseTracker.Application.Services.Implementations
{
    public class ExpenseService : IExpenseService
    {
        private readonly IMapper _mapper;
        private readonly IExpenseRepository _expenseRepository;

        public ExpenseService(
            IMapper mapper,
            IExpenseRepository expenseRepository)
        {
            _mapper = mapper;
            _expenseRepository = expenseRepository;
        }

        public async Task<Expense> AddExpenseAsync(Expense expense)
        {
            expense = await _expenseRepository.AddAsync(expense);
            await _expenseRepository.SaveChangesAsync();

            return expense;
        }

        public async Task<Expense> GetExpenseAsync(int id)
        {
            return await _expenseRepository.GetByIdAsync(id);
        }

        public async Task<IEnumerable<Expense>> GetAllByGroupAsync(int groupId, PaginationModel paginationModel)
        {
            var expenses = await _expenseRepository.ListAsync(
                new ExpensesByGroupIdSpec(groupId), 
                new PaginationSpec<Expense>(paginationModel));

            return expenses;
        }

        public async Task<IEnumerable<Expense>> GetAllByUserAsync(int userId, PaginationModel paginationModel)
        {
            var expenses = await _expenseRepository.ListAsync(
                new ExpensesByUserIdSpec(userId),
                new PaginationSpec<Expense>(paginationModel));

            return expenses;
        }

        public async Task<Expense> GetExpenseByUserAsync(int expenseId, int userId)
        {
            var expense = await _expenseRepository.SingleAsync(
                new ExpenseByIdSpec(expenseId),
                new ExpensesByUserIdSpec(userId));

            return expense;
        }

        public async Task UpdateExpenseAsync(Expense expense)
        {
            var expenseToUpdate = 
                await _expenseRepository.SingleAsync(new ExpenseByIdIncludeSubExpensesSpec(expense.Id));
            var groupId = expenseToUpdate.GroupId;

            var updatedExpense = _mapper.Map(expense, expenseToUpdate);
            updatedExpense.GroupId = groupId;

            if (!expenseToUpdate.SubExpenses.CheckSubExpensesCorrect(updatedExpense.SplitType))
            {
                throw new HttpException("can't update if it cause wrong values in expense");
            }

            await _expenseRepository.UpdateAsync(updatedExpense);
            await _expenseRepository.SaveChangesAsync();
        }

        public async Task DeleteExpenseAsync(int id)
        {
            var expense = await _expenseRepository.GetByIdAsync(id);

            await _expenseRepository.DeleteAsync(expense);
            await _expenseRepository.SaveChangesAsync();
        }

        public async Task<decimal> GetUserMoneyDifferenceAsync(int expenseId, int userId)
        {
            var expense = await _expenseRepository.SingleAsync(
                new ExpenseByIdIncludeSubExpenseUserPaymentSpec(expenseId));

            return expense.CalculateUserMoneyDifference(userId);
        }
       
        public async Task<IEnumerable<UserMoneyDifferenceModel>> GetAllMoneyDifferencesAsync(int expenseId)
        {
            var expense = await _expenseRepository.SingleAsync(
                new ExpenseByIdIncludeSubExpenseUserPaymentSpec(expenseId));

            var moneyDifferences = new List<UserMoneyDifferenceModel>();

            foreach (var subExpense in expense.SubExpenses)
            {
                var userMoneyDifference = new UserMoneyDifferenceModel()
                {
                    UserId = subExpense.UserId,
                    MoneyDifference = expense.CalculateUserMoneyDifference(subExpense.UserId),
                };
                moneyDifferences.Add(userMoneyDifference);
            }

            return moneyDifferences;
        }


        public async Task<bool> CheckExpenseContainsUserAsync(int expenseId, int userId)
        {
            return await GetExpenseByUserAsync(expenseId, userId) is not null;
        }

        public async Task<bool> CheckExpenseInSameGroupWithUserAsync(int expenseId, int userId)
        {
            return await _expenseRepository.SingleAsync(
                new ExpensesInSameGroupWithUserSpec(userId),
                new ExpenseByIdSpec(expenseId)) is not null;
        }
    }
}
