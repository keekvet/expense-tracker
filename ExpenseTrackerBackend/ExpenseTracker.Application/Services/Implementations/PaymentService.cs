﻿using ExpenseTracker.Application.Exceptions;
using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Application.Services.Interfaces;
using ExpenseTracker.Application.Specifications.Payments;
using ExpenseTracker.Application.Specifications.Shared;
using ExpenseTracker.Data.Repositories.Interfaces;
using ExpenseTracker.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExpenseTracker.Application.Services.Implementations
{
    public class PaymentService : IPaymentService
    {
        private readonly IExpenseService _expenseService;
        private readonly IPaymentRepository _paymentRepository;

        public PaymentService(
            IExpenseService expenseService,
            IPaymentRepository paymentRepository)
        {
            _expenseService = expenseService;
            _paymentRepository = paymentRepository;
        }

        public async Task<Payment> AddPaymentAsync(Payment payment)
        {
            var moneyDifferenceSender = 
                await _expenseService.GetUserMoneyDifferenceAsync(payment.ExpenseId, payment.SenderId);

            if(payment.SenderId == payment.ReceiverId)
            {
                throw new HttpException("self payment denied");
            }

            if(moneyDifferenceSender < 0)
            {
                throw new HttpException("sender spent more than split value");
            }
            else if(moneyDifferenceSender < payment.Amount)
            {
                throw new HttpException("sender paying more than expected");
            }

            var moneyDifferenceReceiver =
                await _expenseService.GetUserMoneyDifferenceAsync(payment.ExpenseId, payment.ReceiverId);

            if (moneyDifferenceReceiver > 0)
            {
                throw new HttpException("receiver spent less than split value");
            }
            else if (moneyDifferenceReceiver + payment.Amount > 0)
            {
                throw new HttpException("receiver will receive more than expected");
            }

            payment.Date = DateTime.UtcNow;

            await _paymentRepository.AddAsync(payment);
            await _paymentRepository.SaveChangesAsync();

            return payment;
        }

        public async Task<IEnumerable<Payment>> GetPaymentsByUserAsync(int userId, PaginationModel paginationModel)
        {
            return await _paymentRepository.ListAsync(
                new PaymentsByUserIdSpec(userId),
                new PaginationSpec<Payment>(paginationModel));
        }
    }
}
;