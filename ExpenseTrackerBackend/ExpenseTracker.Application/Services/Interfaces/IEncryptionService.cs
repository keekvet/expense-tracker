﻿using ExpenseTracker.Application.Models;

namespace ExpenseTracker.Application.Services.Interfaces
{
    public interface IEncryptionService
    {
        HashedPasswordModel Encrypt(byte[] data);
        bool PasswordEqualsHash(string password, byte[] hashToCompare, byte[] salt);
    }
}
