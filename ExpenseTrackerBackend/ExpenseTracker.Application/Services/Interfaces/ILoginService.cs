﻿using ExpenseTracker.Application.Models;
using System.Threading.Tasks;

namespace ExpenseTracker.Application.Services.Interfaces
{
    public interface ILoginService
    {
        Task<UserTokenModel> LoginAsync(string userName, string password);
    }
}
