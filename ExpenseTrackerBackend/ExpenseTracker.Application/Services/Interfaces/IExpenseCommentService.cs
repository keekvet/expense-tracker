﻿using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExpenseTracker.Application.Services.Interfaces
{
    public interface IExpenseCommentService
    {
        Task<ExpenseComment> AddExpenseCommentAsync(ExpenseComment expenseComment);
        Task<bool> CheckCommentBelongsToUserAsync(int expenseCommentId, int userId);
        Task DeleteExpenseCommentAsync(int id);
        Task<IEnumerable<ExpenseComment>> GetAllByExpenseIdAsync(int expenseId, PaginationModel paginationModel);
    }
}
