﻿using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExpenseTracker.Application.Services.Interfaces
{
    public interface IPaymentService
    {
        Task<Payment> AddPaymentAsync(Payment payment);
        Task<IEnumerable<Payment>> GetPaymentsByUserAsync(int userId, PaginationModel paginationModel);
    }
}
