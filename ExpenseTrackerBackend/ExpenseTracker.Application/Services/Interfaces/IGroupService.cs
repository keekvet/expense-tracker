﻿using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExpenseTracker.Application.Services.Interfaces
{
    public interface IGroupService
    {
        Task<Group> AddGroupAsync(Group group, int userId);

        Task<Group> GetGroupAsync(int id);
        Task<Group> GetGroupByUserAsync(int groupId, int userId);
        Task<IEnumerable<Group>> GetAllByUserAsync(int id, PaginationModel paginationModel);

        Task UpdateGroupAsync(Group group);
        Task DeleteGroupAsync(int groupId);
        
        Task AddUserToGroupAsync(int userId, int groupId);
        Task RemoveUserFromGroupAsync(int userId, int groupId);
        
        Task<bool> CheckGroupContainsUserAsync(int groupId, int userId);
    }
}
