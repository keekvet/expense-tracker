﻿using ExpenseTracker.Application.Models;
using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExpenseTracker.Application.Services.Interfaces
{
    public interface IUserService
    {
        Task AddUserAsync(RegistrationModel registrationModel);

        Task<User> GetUserAsync(int id);
        Task<IEnumerable<User>> GetAllByGroupAsync(int groupId, PaginationModel paginationModel);
        
        Task UpdateUserAsync(User user);
        Task DeleteUserAsync(int userId);
        Task UpdatePasswordAsync(UserPasswordUpdateModel userPasswordUpdateModel);
    }
}
