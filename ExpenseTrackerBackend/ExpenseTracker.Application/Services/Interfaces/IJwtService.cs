﻿using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Application.Services.Interfaces
{
    public interface IJwtService
    {
        string CreateToken(User user);
    }
}
