﻿using ExpenseTracker.Application.Models;
using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExpenseTracker.Application.Services.Interfaces
{
    public interface ISubExpenseService
    {
        Task<SubExpense> AddSubExpenseAsync(SubExpense subExpense);
        
        Task<IEnumerable<SubExpense>> GetAllByExpenseAsync(int expenseId, PaginationModel paginationModel);
        Task<IEnumerable<SubExpense>> GetAllByUserAsync(int userId, PaginationModel paginationModel);
        Task<SubExpense> GetSubExpenseAsync(int id);

        Task UpdateSubExpenseAsync(SubExpense subExpense);
        Task DeleteSubExpenseAsync(int id);
        
        Task ReplaceInExpenseAsync(int expenseId, IEnumerable<SubExpense> subExpenses);

        Task<bool> CheckSubExpenseInSameGroupWithUserAsync(int subExpenseId, int userId);
    }
}
