﻿using ExpenseTracker.Application.Models;
using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExpenseTracker.Application.Services.Interfaces
{
    public interface IExpenseService
    {
        Task<Expense> AddExpenseAsync(Expense expense);
        
        Task<Expense> GetExpenseAsync(int id);
        Task<Expense> GetExpenseByUserAsync(int expenseId, int userId);
        Task<IEnumerable<Expense>> GetAllByGroupAsync(int groupId, PaginationModel paginationModel);
        Task<IEnumerable<Expense>> GetAllByUserAsync(int userId, PaginationModel paginationModel);
        Task<IEnumerable<UserMoneyDifferenceModel>> GetAllMoneyDifferencesAsync(int expenseId);
        Task<decimal> GetUserMoneyDifferenceAsync(int expenseId, int userId);

        Task UpdateExpenseAsync(Expense expense);
        Task DeleteExpenseAsync(int id);

        Task<bool> CheckExpenseContainsUserAsync(int expenseId, int userId);
        Task<bool> CheckExpenseInSameGroupWithUserAsync(int expenseId, int userId);
    }
}
