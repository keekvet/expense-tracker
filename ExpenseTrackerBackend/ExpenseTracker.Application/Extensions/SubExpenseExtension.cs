﻿using ExpenseTracker.Application.Exceptions;
using ExpenseTracker.Domain.Enums;
using ExpenseTracker.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace ExpenseTracker.Application.Extensions
{
    public static class SubExpenseExtension
    {
        public static decimal GetSplitDifference(this SubExpense subExpense, decimal totalSpent)
        {
            decimal borrowed = subExpense.Expense.SplitType switch
            {
                ExpenseSplitType.Absolute => subExpense.SplitValue,
                ExpenseSplitType.Percent => (subExpense.SplitValue / 100) * totalSpent,
                _ => throw new HttpException(httpStatusCode: HttpStatusCode.InternalServerError)
            };

            return borrowed - subExpense.SpentAmount;
        }

        public static bool CheckSubExpensesCorrect(
            this IEnumerable<SubExpense> subExpenses, 
            ExpenseSplitType splitType)
        {
            if (!subExpenses.Any())
            {
                return true;
            }

            if (subExpenses.GroupBy(s => s.ExpenseId).Count() > 1)
            {
                return false;
            }

            var spendedAmount = decimal.Zero;
            var splitValueSum = decimal.Zero;

            foreach (var subExpense in subExpenses)
            {
                spendedAmount += subExpense.SpentAmount;
                splitValueSum += subExpense.SplitValue;
            }

            return (splitType) switch
            {
                ExpenseSplitType.Absolute => spendedAmount == splitValueSum,
                ExpenseSplitType.Percent => splitValueSum == 100,
                _ => false,
            };
        }
    }
}
