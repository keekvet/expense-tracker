﻿using ExpenseTracker.Application.Exceptions;
using ExpenseTracker.Domain.Models;
using System.Linq;

namespace ExpenseTracker.Application.Extensions
{
    public static class ExpenseExtension
    {
        public static decimal GetSpended(this Expense expense)
        {
            return expense.SubExpenses
                .Select(s => s.SpentAmount)
                .Aggregate((s1, s2) => s1 + s2);
        }

        public static bool ContainsUser(this Expense expense, int userId)
        {
            return expense.SubExpenses
                .Where(s => s.UserId == userId).Any();
        }

        public static decimal CalculateUserMoneyDifference(this Expense expense, int userId)
        {

            if (!expense.ContainsUser(userId))
            {
                throw new HttpException("user not in expense");
            }

            var totalExpenseSpended = expense.GetSpended();

            var userSubExpense = expense.SubExpenses.Where(s => s.UserId == userId).FirstOrDefault();

            var userDifference = userSubExpense.GetSplitDifference(totalExpenseSpended);

            var userPayments = expense.Payments.Where(p => p.SenderId == userId || p.ReceiverId == userId);

            foreach (var payment in userPayments)
            {
                if (payment.SenderId == userId)
                {
                    userDifference -= payment.Amount;
                }
                else
                {
                    userDifference += payment.Amount;
                }
            }

            return decimal.Round(userDifference, 2);
        }
    }
}
