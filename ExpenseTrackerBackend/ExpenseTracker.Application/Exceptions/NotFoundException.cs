﻿using System.Net;

namespace ExpenseTracker.Application.Exceptions
{
    public class NotFoundException : HttpException
    {
        public NotFoundException(
            string notFoundEntityName, 
            HttpStatusCode httpStatusCode = HttpStatusCode.NotFound) 
            : base($"{notFoundEntityName} not found", httpStatusCode)
        {
        }
    }
}
