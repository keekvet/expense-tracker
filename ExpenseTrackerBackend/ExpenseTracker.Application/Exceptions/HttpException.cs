﻿using System;
using System.Net;

namespace ExpenseTracker.Application.Exceptions
{
    public class HttpException : Exception
    {
        public HttpStatusCode HttpStatusCode { get; private set; }
        
        public HttpException(
            string message = "", 
            HttpStatusCode httpStatusCode = HttpStatusCode.BadRequest) 
            : base(message)
        {
            HttpStatusCode = httpStatusCode;
        }
    }
}
