﻿using System.Collections.Generic;

namespace ExpenseTracker.Application.Models.Pagination
{
    public class PaginationModel
    {
        public int Count { get; set; } = 100;
        public int Page { get; set; } = 1;
        public bool Ascending { get; set; } = true;
        public string OrderBy { get; set; }
        public Dictionary<string, string> Filters { get; set; } = new Dictionary<string, string>();
    }
}
