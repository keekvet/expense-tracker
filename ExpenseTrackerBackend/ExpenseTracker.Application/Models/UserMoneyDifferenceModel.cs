﻿namespace ExpenseTracker.Application.Models
{
    public class UserMoneyDifferenceModel
    {
        public int UserId { get; set; }
        public decimal MoneyDifference { get; set; }

    }
}
