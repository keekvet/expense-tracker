﻿namespace ExpenseTracker.Application.Models
{
    public class UserPasswordUpdateModel
    {
        public int UserId { get; set; }
        public string NewPassword { get; set; }
        public string OldPassword { get; set; }
    }
}
