﻿namespace ExpenseTracker.Application.Models
{
    public class HashedPasswordModel
    {
        public byte[] HashPassword { get; set; }
        public byte[] Salt { get; set; }

    }
}
