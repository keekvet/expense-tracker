﻿using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Application.Models
{
    public class UserTokenModel
    {
        public string Token { get; set; }
        public User User { get; set; }

    }
}
