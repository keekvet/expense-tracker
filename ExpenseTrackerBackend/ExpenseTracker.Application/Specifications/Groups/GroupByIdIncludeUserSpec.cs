﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Application.Specifications.Groups
{
    public class GroupByIdIncludeUsersSpec : Specification<Group>
    {
        public GroupByIdIncludeUsersSpec(int groupId)
        {
            Query
                .Where(g => g.Id == groupId)
                .Include(g => g.Users);
        }        
    }
}
