﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;
using System.Linq;

namespace ExpenseTracker.Application.Specifications.Groups
{
    public class GroupsByUserIdSpec : Specification<Group>
    {
        public GroupsByUserIdSpec(int userId)
        {
            Query
                .Include(g => g.Users)
                .Where(g => g.Users.Where(u => u.Id == userId).Any());
        }
    }
}
