﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Application.Specifications.Payments
{
    public class PaymentsByUserIdSpec : Specification<Payment>
    {
        public PaymentsByUserIdSpec(int userId)
        {
            Query
                .Where(p => p.SenderId == userId || p.ReceiverId == userId);
        }
    }
}
