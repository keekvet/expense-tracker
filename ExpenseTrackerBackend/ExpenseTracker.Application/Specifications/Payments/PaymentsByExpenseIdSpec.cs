﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Application.Specifications.Payments
{
    public class PaymentsByExpenseIdSpec : Specification<Payment>
    {
        public PaymentsByExpenseIdSpec(int expenseId)
        {
            Query
                .Where(p => p.ExpenseId == expenseId);
        }
    }
}
