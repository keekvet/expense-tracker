﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Application.Specifications.Payments
{
    public class PaymentsByExpenseIdAndSenderIdSpec : Specification<Payment>
    {
        public PaymentsByExpenseIdAndSenderIdSpec(int expenseId, int senderId)
        {
            Query
                .Where(p => p.ExpenseId == expenseId && p.SenderId == senderId);
        }
    }
}
