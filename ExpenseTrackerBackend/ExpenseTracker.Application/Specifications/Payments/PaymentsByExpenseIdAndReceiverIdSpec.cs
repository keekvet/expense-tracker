﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Application.Specifications.Payments
{
    public class PaymentsByExpenseIdAndReceiverIdSpec : Specification<Payment>
    {
        public PaymentsByExpenseIdAndReceiverIdSpec(int expenseId, int receiverId)
        {
            Query
                .Where(p => p.ExpenseId == expenseId && p.ReceiverId == receiverId);
        }
    }
}
