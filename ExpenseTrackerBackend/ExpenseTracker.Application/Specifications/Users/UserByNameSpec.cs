﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Application.Specifications.Users
{
    public class UserByNameSpec : Specification<User>
    {
        public UserByNameSpec(string name)
        {
            Query
                .Where(u => u.Name.Equals(name));
        }
    }
}
