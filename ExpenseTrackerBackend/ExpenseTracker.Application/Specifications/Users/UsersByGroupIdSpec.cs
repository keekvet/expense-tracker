﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;
using System.Linq;

namespace ExpenseTracker.Application.Specifications.Users
{
    public class UsersByGroupIdSpec : Specification<User>
    {
        public UsersByGroupIdSpec(int id)
        {
            Query
                .Include(u => u.Groups)
                .Where(u => u.Groups.Where(g => g.Id == id).Any());
        }        
    }
}
