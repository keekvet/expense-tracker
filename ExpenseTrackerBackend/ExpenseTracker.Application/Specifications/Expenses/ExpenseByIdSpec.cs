﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Application.Specifications.Expenses
{
    public class ExpenseByIdSpec : Specification<Expense>
    {
        public ExpenseByIdSpec(int id)
        {
            Query
                .Where(e => e.Id == id);
        }
    }
}
