﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;
using System.Linq;

namespace ExpenseTracker.Application.Specifications.Expenses
{
    public class ExpenseByIdIncludeSubExpenseUserPaymentSpec : Specification<Expense>
    {
        public ExpenseByIdIncludeSubExpenseUserPaymentSpec(int id)
        {
            Query
                .Where(e => e.Id == id)
                .Include(e => e.Payments)
                .Include(e => e.SubExpenses)
                .ThenInclude(s => s.User);
        }

        public ExpenseByIdIncludeSubExpenseUserPaymentSpec(int expenseId, int userId)
        {
            Query
                .Where(e => e.Id == expenseId)
                .Include(e => e.Payments)
                .Include(e => e.SubExpenses)
                .ThenInclude(s => s.User)
                .Where(e => e.SubExpenses.Where(s => s.UserId == userId).Any());
        }
    }
}
