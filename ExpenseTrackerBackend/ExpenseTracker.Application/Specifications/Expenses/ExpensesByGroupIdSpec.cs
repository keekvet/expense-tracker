﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Application.Specifications.Expenses
{
    public class ExpensesByGroupIdSpec : Specification<Expense>
    {
        public ExpensesByGroupIdSpec(int groupId)
        {
            Query
                .Include(e => e.Group)
                .Where(e => e.Group.Id == groupId);
        }
    }
}
 