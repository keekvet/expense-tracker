﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Application.Specifications.Expenses
{
    public class ExpenseByIdIncludeSubExpensesSpec : Specification<Expense>
    {
        public ExpenseByIdIncludeSubExpensesSpec(int id)
        {
            Query
                .Where(e => e.Id == id)
                .Include(e => e.SubExpenses);
        }
    }
}
