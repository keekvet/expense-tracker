﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;
using System.Linq;

namespace ExpenseTracker.Application.Specifications.Expenses
{
    public class ExpensesInSameGroupWithUserSpec : Specification<Expense>
    {
        public ExpensesInSameGroupWithUserSpec(int userId)
        {
            Query
                .Include(e => e.Group)
                .ThenInclude(g => g.Users)
                .Where(e => e.Group.Users.Where(u => u.Id == userId).Any());
        }
    }
}
