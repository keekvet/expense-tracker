﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Application.Specifications.Expenses
{
    public class ExpensesIncludeSubExpensesUsersPaymentsSpec : Specification<Expense>
    {
        public ExpensesIncludeSubExpensesUsersPaymentsSpec()
        {
            Query
                .Include(e => e.Payments)
                .Include(e => e.SubExpenses)
                .ThenInclude(s => s.User);
        }
    }
}
