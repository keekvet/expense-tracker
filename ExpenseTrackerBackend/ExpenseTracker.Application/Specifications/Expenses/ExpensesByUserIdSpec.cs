﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;
using System.Linq;

namespace ExpenseTracker.Application.Specifications.Expenses
{
    public class ExpensesByUserIdSpec : Specification<Expense>
    {
        public ExpensesByUserIdSpec(int userId)
        {
            Query
                .Include(e => e.SubExpenses)
                .Where(e => e.SubExpenses.Any(s => s.UserId == userId));
        }
    }
}
