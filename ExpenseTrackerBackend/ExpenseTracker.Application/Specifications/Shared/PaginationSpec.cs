﻿using Ardalis.Specification;
using ExpenseTracker.Application.Models.Pagination;
using System;
using System.Linq.Expressions;
using System.Reflection;

namespace ExpenseTracker.Application.Specifications.Shared
{
    public class PaginationSpec<TEntity> : Specification<TEntity>
    {
        public PaginationSpec(PaginationModel paginationModel)
        {
            var propertyFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.IgnoreCase;

            var parameter = Expression.Parameter(typeof(TEntity));

            var containsMethod = typeof(string).GetMethod("Contains", new[] { typeof(string) });

            foreach (var item in paginationModel.Filters)
            {
                var propertyInfo = typeof(TEntity).GetProperty(item.Key, propertyFlags);

                if (propertyInfo is null 
                    || item.Value is null 
                    || propertyInfo.PropertyType != typeof(string))
                    continue;

                var constant = Expression.Constant(item.Value, item.Value.GetType());
                var property = Expression.Property(parameter, item.Key);
                
                var containsExpression = Expression.Call(property, containsMethod, constant);
                
                Query.Where(Expression.Lambda<Func<TEntity, bool>>(containsExpression, parameter));
            }


            if (paginationModel.OrderBy is not null)
            {
                var sortByProperty = typeof(TEntity).GetProperty(paginationModel.OrderBy, propertyFlags);

                if (sortByProperty is not null 
                    && sortByProperty.PropertyType.GetInterface(nameof(IComparable)) is not null)
                {

                    var property = Expression.Property(parameter, sortByProperty.Name);
                    var convertProperty = Expression.Convert(property, typeof(object));
                    var propertyLambda = Expression.Lambda<Func<TEntity, object>> (convertProperty, parameter);


                    if (paginationModel.Ascending)
                        Query.OrderBy(propertyLambda);
                    else
                        Query.OrderByDescending(propertyLambda);
                }
            }

            Query
                .Skip((paginationModel.Page - 1) * paginationModel.Count)
                .Take(paginationModel.Count);
        }
    }
}
