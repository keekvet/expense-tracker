﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Application.Specifications.ExpenseComments
{
    public class ExpenseCommentByIdAndUserIdSpec : Specification<ExpenseComment>
    {
        public ExpenseCommentByIdAndUserIdSpec(int expenseCommentId, int userId)
        {
            Query
                .Include(e => e.User)
                .Where(e => e.Id == expenseCommentId && e.UserId == userId);
        }
    }
}
