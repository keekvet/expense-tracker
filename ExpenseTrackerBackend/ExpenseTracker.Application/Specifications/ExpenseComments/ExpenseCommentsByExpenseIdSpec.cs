﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Application.Specifications.ExpenseComments
{
    public class ExpenseCommentsByExpenseIdSpec : Specification<ExpenseComment>
    {
        public ExpenseCommentsByExpenseIdSpec(int expenseId)
        {
            Query
                .Where(e => e.ExpenseId == expenseId);
        }
    }
}
