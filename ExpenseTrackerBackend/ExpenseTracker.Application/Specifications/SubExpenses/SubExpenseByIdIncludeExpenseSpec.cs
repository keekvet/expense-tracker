﻿using Ardalis.Specification;

namespace ExpenseTracker.Application.Specifications.SubExpenses
{
    public class SubExpenseByIdIncludeExpenseSpec : SubExpenseByIdSpec
    {
        public SubExpenseByIdIncludeExpenseSpec(int id) : base(id)
        {
            Query
                .Include(s => s.Expense);
        }
    }
}
