﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Application.Specifications.SubExpenses
{
    public class SubExpensesByExpenseIdSpec : Specification<SubExpense>
    {
        public SubExpensesByExpenseIdSpec(int expenseId)
        {
            Query
                .Include(s => s.Expense)
                .Where(s => s.ExpenseId == expenseId);
        }
    }
}
