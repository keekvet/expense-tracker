﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Application.Specifications.SubExpenses
{
    public class SubExpensesByUserIdSpec : Specification<SubExpense>
    {
        public SubExpensesByUserIdSpec(int userId)
        {
            Query
                .Where(s => s.UserId == userId);
        }
    }
}
