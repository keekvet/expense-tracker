﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;
using System.Linq;

namespace ExpenseTracker.Application.Specifications.SubExpenses
{
    public class SubExpensesInSameGroupWithUserSpec : Specification<SubExpense>
    {
        public SubExpensesInSameGroupWithUserSpec(int userId)
        {
            Query
                .Include(s => s.Expense)
                .ThenInclude(e => e.Group)
                .ThenInclude(e => e.Users)
                .Where(s => s.Expense.Group.Users.Any(u => u.Id == userId));
        }
    }
}
