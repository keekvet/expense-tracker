﻿using Ardalis.Specification;
using ExpenseTracker.Domain.Models;

namespace ExpenseTracker.Application.Specifications.SubExpenses
{
    public class SubExpenseByIdSpec : Specification<SubExpense>
    {
        public SubExpenseByIdSpec(int id)
        {
            Query
                .Where(s => s.Id == id);
        }        
    }
}
