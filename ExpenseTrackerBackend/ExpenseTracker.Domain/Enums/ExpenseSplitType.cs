﻿
namespace ExpenseTracker.Domain.Enums
{
    public enum ExpenseSplitType
    {
        Absolute,
        Percent
    }
}
