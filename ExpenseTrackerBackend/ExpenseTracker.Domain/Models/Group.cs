﻿using ExpenseTracker.Domain.Models.Interfaces;
using System.Collections.Generic;

namespace ExpenseTracker.Domain.Models
{
    public class Group : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<User> Users { get; private set; } = new List<User>();
        public ICollection<Expense> Expenses { get; private set; } = new List<Expense>();
    }
}
