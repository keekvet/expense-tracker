﻿using ExpenseTracker.Domain.Models.Interfaces;
using System.Collections.Generic;

namespace ExpenseTracker.Domain.Models
{
    public class User : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public byte[] PasswordHash { get; set; }
        public byte[] Salt { get; set; }

        public ICollection<Group> Groups { get; private set; } = new List<Group>(); 
        public ICollection<Payment> IncomePayments { get; private set; } = new List<Payment>(); 
        public ICollection<Payment> OutputPayments { get; private set; } = new List<Payment>(); 
        public ICollection<SubExpense> SubExpenses { get; private set; } = new List<SubExpense>(); 
        public ICollection<ExpenseComment> Comments { get; private set; } = new List<ExpenseComment>(); 
    }
}
