﻿using ExpenseTracker.Domain.Models.Interfaces;
using System.Collections.Generic;

namespace ExpenseTracker.Domain.Models
{
    public class SubExpense : IEntity<int>
    {
        public int Id { get; set; }
        public decimal SplitValue { get; set; }
        public decimal SpentAmount{ get; set; }

        public int ExpenseId { get; set; }
        public Expense Expense { get; set; }
        
        public int UserId { get; set; }
        public User User { get; set; }

    }
}
