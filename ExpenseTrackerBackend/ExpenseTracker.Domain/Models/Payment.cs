﻿using ExpenseTracker.Domain.Models.Interfaces;
using System;

namespace ExpenseTracker.Domain.Models
{
    public class Payment : IEntity<int>
    {
        public int Id { get; set; }
        public string Note { get; set; }
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }

        public int SenderId { get; set; }
        public User Sender { get; set; }
        
        public int ReceiverId { get; set; }
        public User Receiver { get; set; }
        
        public int ExpenseId { get; set; }
        public Expense Expense { get; set; }
        
    }
}
