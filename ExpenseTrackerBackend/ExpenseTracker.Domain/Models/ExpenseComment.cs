﻿using ExpenseTracker.Domain.Models.Interfaces;
using System;

namespace ExpenseTracker.Domain.Models
{
    public class ExpenseComment : IEntity<int>
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }

        public int ExpenseId { get; set; }
        public Expense Expense { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}
