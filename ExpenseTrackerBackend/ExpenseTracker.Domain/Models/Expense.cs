﻿using ExpenseTracker.Domain.Enums;
using ExpenseTracker.Domain.Models.Interfaces;
using System;
using System.Collections.Generic;

namespace ExpenseTracker.Domain.Models
{
    public class Expense : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; } = DateTime.UtcNow;
        public ExpenseSplitType SplitType { get; set; }

        public int GroupId { get; set; }
        public Group Group { get; set; }

        public ICollection<SubExpense> SubExpenses { get; private set; } = new List<SubExpense>();
        public ICollection<ExpenseComment> Comments { get; private set; } = new List<ExpenseComment>();
        public ICollection<Payment> Payments { get; private set; } = new List<Payment>();

        public bool ContainsUser()
        {
            throw new NotImplementedException();
        }
    }
}
