﻿
namespace ExpenseTracker.Domain.Models.Interfaces
{
    public interface IEntity<TId>
    {
        public TId Id { get; set; }
    }
}
