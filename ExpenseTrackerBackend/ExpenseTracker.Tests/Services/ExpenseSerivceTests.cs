﻿using Ardalis.Specification;
using ExpenseTracker.Application.Exceptions;
using ExpenseTracker.Application.Models;
using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Application.Services.Implementations;
using ExpenseTracker.Data.Repositories.Interfaces;
using ExpenseTracker.Domain.Enums;
using ExpenseTracker.Domain.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ExpenseTracker.Tests.Services
{
    public class ExpenseSerivceTests : BaseServiceTests
    {
        private readonly ExpenseService _expenseService;
        private readonly Mock<IExpenseRepository> _expenseRepositoryMock;
        public ExpenseSerivceTests()
        {
            _expenseRepositoryMock = new Mock<IExpenseRepository>();

            _expenseService = new ExpenseService(_mapper, _expenseRepositoryMock.Object);
        }

        [Fact]
        public async Task AddExpenseAsync_ExpenseIsValid_ReturnsAddedExpense()
        {
            //arrange 
            var expense = GetExpenses()[0];
            _expenseRepositoryMock
                .Setup(e => e.AddAsync(It.IsAny<Expense>()))
                .Returns(Task.FromResult(expense));

            //act
            var result = await _expenseService.AddExpenseAsync(expense);

            //assert
            Assert.Equal(expense, result);
        }

        [Fact]
        public async Task GetExpenseAsync_ExpenseExist_ReturnsExpense()
        {
            //arrange
            int expenseId = 1;
            var expense = GetExpenses().Where(e => e.Id == expenseId).Single();
            _expenseRepositoryMock
                .Setup(e => e.GetByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(expense));

            //act
            var result = await _expenseService.GetExpenseAsync(expenseId);

            //assert
            Assert.Equal(expense, result);
        }

        [Fact]
        public async Task GetAllByGroupAsync_GroupExists_ReturnsExpensesInGroup()
        {
            //arrange
            int groupId = 1;
            var expenses = GetExpenses().Where(e => e.GroupId == groupId);

            _expenseRepositoryMock
                .Setup(e => e.ListAsync(It.IsAny<ISpecification<Expense>>(), It.IsAny<ISpecification<Expense>>()))
                .Returns(Task.FromResult(expenses.ToList()));

            //act
            var result = await _expenseService.GetAllByGroupAsync(groupId, new PaginationModel());

            //assert
            Assert.Equal(expenses, result);
        }

        [Fact]
        public async Task GetAllByUserAsync_UserExists_ReturnsUserExpenses()
        {
            //arrange
            int userId = 1;
            var expenses = GetExpenses().Where(e => e.SubExpenses.Any(s => s.UserId == userId));

            _expenseRepositoryMock
                .Setup(e => e.ListAsync(It.IsAny<ISpecification<Expense>>(), It.IsAny<ISpecification<Expense>>()))
                .Returns(Task.FromResult(expenses.ToList()));

            //act
            var result = await _expenseService.GetAllByUserAsync(userId, new PaginationModel());

            //assert
            Assert.Equal(expenses, result);
        }

        [Fact]
        public async Task GetExpenseByUserAsync_UserExistsAndExpenseExist_ReturnsUserExpense()
        {
            //arrange
            int userId = 1;
            int expenseId = 1;
            var expense = GetExpenses()
                .FirstOrDefault(e => e.SubExpenses.Any(s => s.UserId == userId && s.ExpenseId == expenseId));

            _expenseRepositoryMock
                .Setup(e => e.SingleAsync(It.IsAny<ISpecification<Expense>>(), It.IsAny<ISpecification<Expense>>()))
                .Returns(Task.FromResult(expense));

            //act
            var result = await _expenseService.GetExpenseByUserAsync(userId, expenseId);

            //assert
            Assert.Equal(expense, result);
        }

        [Fact]
        public async Task UpdateExpenseAsync_ExpenseExistAndSubExpensesCorrect_ReturnsVoid()
        {
            //arrange
            var expense = GetExpenses()[0];
                
            _expenseRepositoryMock
                .Setup(e => e.SingleAsync(It.IsAny<ISpecification<Expense>>()))
                .Returns(Task.FromResult(expense));

            //act
            await _expenseService.UpdateExpenseAsync(expense);

            //assert
            _expenseRepositoryMock.Verify(e => e.SaveChangesAsync(default), Times.Once());
        }

        [Fact]
        public async Task UpdateExpenseAsync_ExpenseExistAndSubExpensesNotCorrect_ThrowsHttpException()
        {
            //arrange
            var expense = GetExpenses()[1];
            expense.SplitType = ExpenseSplitType.Percent;

            _expenseRepositoryMock
                .Setup(e => e.SingleAsync(It.IsAny<ISpecification<Expense>>()))
                .Returns(Task.FromResult(expense));

            //act
            //assert
            await Assert.ThrowsAsync<HttpException>(() => _expenseService.UpdateExpenseAsync(expense));
        }

        [Fact]
        public async Task DeleteExpenseAsync_ExpenseExists_ReturnsVoid()
        {
            //arrange
            var expenseId = 1;

            _expenseRepositoryMock
                .Setup(e => e.GetByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(GetExpenses().FirstOrDefault(e => e.Id == expenseId)));

            _expenseRepositoryMock
                .Setup(e => e.DeleteAsync(It.IsAny<Expense>()))
                .Returns(Task.CompletedTask);

            //act
            await _expenseService.DeleteExpenseAsync(expenseId);

            //assert
            _expenseRepositoryMock.Verify(e => e.SaveChangesAsync(default), Times.Once());
        }

        [Theory]
        [InlineData(1, 1, 10)]
        [InlineData(2, 1, 0)]
        public async Task GetUserMoneyDifferenceAsync_ExpenseExistAndUserInExpense_ReturnsMoneyDifference(int expenseId, 
                                                                                                          int userId, 
                                                                                                          decimal expected)
        {
            //arrange
            _expenseRepositoryMock
                 .Setup(e => e.SingleAsync(It.IsAny<ISpecification<Expense>>()))
                 .Returns(Task.FromResult(GetExpenses().FirstOrDefault(e => e.Id == expenseId)));

            //act
            var result = await _expenseService.GetUserMoneyDifferenceAsync(expenseId, userId);

            //assert
            Assert.Equal(expected, result);
        }
      
        [Fact]
        public async Task GetAllMoneyDifferenceAsync_ExpenseExist_ReturnsAllMoneyDifference()
        {
            //arrange
            var expenseId = 1;
            _expenseRepositoryMock
                .Setup(e => e.SingleAsync(It.IsAny<ISpecification<Expense>>()))
                .Returns(Task.FromResult(GetExpenses().FirstOrDefault(e => e.Id == expenseId)));

            //act
            var result = await _expenseService.GetAllMoneyDifferencesAsync(expenseId);

            //assert
            var usersDifferences = new List<UserMoneyDifferenceModel>
            {
                new UserMoneyDifferenceModel() { UserId = 1, MoneyDifference = (decimal)10.0 },
                new UserMoneyDifferenceModel() { UserId = 2, MoneyDifference = (decimal)-10.0 },
            };

            for (int i = 0; i < result.Count(); i++)
            {
                Assert.Equal(usersDifferences[i].UserId, result.ElementAt(i).UserId);
                Assert.Equal(usersDifferences[i].MoneyDifference, result.ElementAt(i).MoneyDifference);
            }
        }

        [Fact]
        public async Task CheckExpenseInSameGroupWithUserAsync_ExpenseInSameGroupWithUser_ReturnsTrue()
        {
            //arrange
            int expenseId = 1;
            int userId = 1;
            var expense = GetExpenses()[0];

            _expenseRepositoryMock
                .Setup(e => e.SingleAsync(It.IsAny<ISpecification<Expense>>(), It.IsAny<ISpecification<Expense>>()))
                .Returns(Task.FromResult(expense));

            //act
            var result = await _expenseService.CheckExpenseInSameGroupWithUserAsync(expenseId, userId);

            //assert
            Assert.True(result);
        }

        [Fact]
        public async Task CheckExpenseContainsUserAsync_ExpenseContainsUser_ReturnsTrue()
        {
            //arrange
            int userId = 1;
            int expenseId = 1;
            var expense = GetExpenses()
                .FirstOrDefault(e => e.SubExpenses.Any(s => s.UserId == userId && s.ExpenseId == expenseId));

            _expenseRepositoryMock
                .Setup(e => e.SingleAsync(It.IsAny<ISpecification<Expense>>(), It.IsAny<ISpecification<Expense>>()))
                .Returns(Task.FromResult(expense));

            //act
            var result = await _expenseService.CheckExpenseContainsUserAsync(expenseId, userId);

            //assert
            Assert.True(result);
        }

        private List<Payment> GetPayments()
        {
            var users = GetUsers();

            return new List<Payment>()
            {
                new Payment()
                {
                    Id = 1,
                    Note = "asdf",
                    Amount = 10,
                    Date = DateTime.UtcNow,

                    SenderId = 1,
                    Sender = users[0],

                    ReceiverId = 2,
                    Receiver = users[1],
                },

                new Payment()
                {
                    Id = 2,
                    Note = "asdf2",
                    Amount = 150,
                    Date = DateTime.UtcNow,

                    SenderId = 2,
                    Sender = users[1],

                    ReceiverId = 1,
                    Receiver = users[0],
                },
            };
        }

        private List<User> GetUsers()
        {
            return new List<User>()
            {
                new User(){ Id = 1, Name = "1"},
                new User(){ Id = 2, Name = "2"},
                new User(){ Id = 3, Name = "3"}
            };
        }

        private List<Expense> GetExpenses()
        {
            var subExpenses = GetSubExpenses();
            var payments = GetPayments();

            var expenses = new List<Expense>()
            {
                new Expense()
                {
                    Id = 1,
                    Name = "exp-1",
                    SplitType = ExpenseSplitType.Percent,
                },

                new Expense()
                {
                    Id = 2,
                    Name = "exp-2",
                    SplitType = ExpenseSplitType.Absolute
                }
            };

            expenses[0].SubExpenses.Add(subExpenses[0]);
            expenses[0].SubExpenses.Add(subExpenses[1]);

            expenses[1].SubExpenses.Add(subExpenses[2]);
            expenses[1].SubExpenses.Add(subExpenses[3]);

            expenses[0].Payments.Add(payments[0]);
            expenses[1].Payments.Add(payments[1]);

            foreach (var expense in expenses)
            {
                expense.GroupId = 1;
                foreach (var subExpense in expense.SubExpenses)
                {
                    subExpense.Expense = expense;
                    subExpense.ExpenseId = expense.Id;
                }
            }

            return expenses;
        }

        private List<SubExpense> GetSubExpenses()
        {
            List<User> users = GetUsers();

            return new List<SubExpense>()
            {
                new SubExpense()
                {
                    Id = 1,
                    SpentAmount = 0,
                    SplitValue = 50,

                    UserId = 1,
                    User = users[0]
                },
              
                new SubExpense()
                {
                    Id = 2,
                    SpentAmount = 40,
                    SplitValue = 50,

                    UserId = 2,
                    User = users[1]
                },

                new SubExpense()
                {
                    Id = 3,
                    SpentAmount = 300,
                    SplitValue = 150,

                    UserId = 1,
                    User = users[0]
                },

                new SubExpense()
                {
                    Id = 4,
                    SpentAmount = 100,
                    SplitValue = 250,

                    UserId = 2,
                    User = users[1]
                }
            };
        }
    }
}