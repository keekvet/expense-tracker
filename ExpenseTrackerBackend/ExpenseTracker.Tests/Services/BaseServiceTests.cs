﻿using AutoMapper;
using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.WebApi.MapperProfiles;
using System.Collections.Generic;

namespace ExpenseTracker.Tests.Services
{
    public class BaseServiceTests
    {
        protected readonly IMapper _mapper;

        public BaseServiceTests()
        {
            _mapper = new Mapper(new MapperConfiguration(opt => opt.AddProfiles(
                new List<Profile>
                {
                    new UserProfile(),
                    new GroupProfile(),
                    new ExpenseProfile(),
                    new PaymentProfile(),
                    new UserTokenProfile(),
                    new SubExpenseProfile(),
                    new RegistrationProfile(),
                    new ExpenseCommentProfile(),
                })));
        }
    }
}
