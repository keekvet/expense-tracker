﻿using ExpenseTracker.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Net;

namespace ExpenseTracker.WebApi.Filters
{
    public class ValidationResponseFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var errors = context.ModelState.Values.Where(v => v.Errors.Count > 0)
                        .SelectMany(v => v.Errors)
                        .Select(v => v.ErrorMessage)
                        .ToList();

                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;

                context.Result = new JsonResult(new ErrorDto() {
                    Type = "ValidationError",
                    Message = errors
                });
            }
        }
    }
}
