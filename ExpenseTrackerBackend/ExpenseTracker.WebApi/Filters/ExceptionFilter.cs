﻿using ExpenseTracker.Application.Exceptions;
using ExpenseTracker.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;

namespace ExpenseTracker.WebApi.Filters
{
    public class ExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            var responseCode = HttpStatusCode.InternalServerError;
            var message = context.Exception.Message;

            HttpException httpException = context.Exception as HttpException;

            if(httpException is not null)
            {
                responseCode = httpException.HttpStatusCode;
                message = httpException.Message;
            }

            context.HttpContext.Response.StatusCode = (int)responseCode;

            
            context.Result = new JsonResult(
                new ErrorDto() 
                { 
                    Type = context.Exception.GetType().Name,
                    Message = message 
                });
        }
    }
}
