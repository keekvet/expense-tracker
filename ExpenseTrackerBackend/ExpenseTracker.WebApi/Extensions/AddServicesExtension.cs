﻿using ExpenseTracker.Application.ConfigurationSections;
using ExpenseTracker.Application.Services;
using ExpenseTracker.Application.Services.Implementations;
using ExpenseTracker.Application.Services.Interfaces;
using ExpenseTracker.Data;
using ExpenseTracker.Data.Repositories.Implementations;
using ExpenseTracker.Data.Repositories.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace ExpenseTracker.WebApi.Extensions
{
    public static class AddServicesExtension
    {
        public static IServiceCollection AddCustomServices(this IServiceCollection services, IConfiguration config)
        {
            services.AddRepositories();
            services.AddConfiguration(config);

            services.AddScoped<IJwtService, JwtService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<IGroupService, GroupService>();
            services.AddScoped<IExpenseService, ExpenseService>();
            services.AddScoped<IPaymentService, PaymentService>();
            services.AddScoped<ISubExpenseService, SubExpenseService>();
            services.AddScoped<IEncryptionService, EncryptionService>();
            services.AddScoped<IExpenseCommentService, ExpenseCommentService>();

            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IGroupRepository, GroupRepository>();
            services.AddScoped<IExpenseRepository, ExpenseRepository>();
            services.AddScoped<IPaymentRepository, PaymentRepository>();
            services.AddScoped<ISubExpenseRepository, SubExpenseRepository>();
            services.AddScoped<IExpenseCommentRepository, ExpenseCommentRepository>();

            return services;
        }

        public static IServiceCollection AddConfiguration(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<JwtConfiguration>(config.GetSection(nameof(JwtConfiguration)));
            services.Configure<GroupConfiguration>(config.GetSection(nameof(GroupConfiguration)));
            services.Configure<EncryptionConfiguration>(config.GetSection(nameof(EncryptionConfiguration)));
            services.Configure<PaginationConfiguration>(config.GetSection(nameof(PaginationConfiguration)));

            return services;
        }

        public static IServiceCollection AddJwtAuthentication(
            this IServiceCollection services, 
            JwtConfiguration jwtConfiguration)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(opt =>
                {
                    opt.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = jwtConfiguration.Issuer,
                        ValidAudience = jwtConfiguration.Audience,
                        IssuerSigningKey =
                            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtConfiguration.SigningKey))
                    };
                });

            return services;
        }

        public static IServiceCollection AddExpenseContext(this IServiceCollection services, IConfiguration config)
        {
            services.AddDbContext<ExpenseContext>(opt =>
            {
                opt.UseSqlServer(config.GetConnectionString("Default"),
                                 a => a.MigrationsAssembly(typeof(ExpenseContext).Assembly.FullName));
               
                opt.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            });

            return services;
        }
    }
}
