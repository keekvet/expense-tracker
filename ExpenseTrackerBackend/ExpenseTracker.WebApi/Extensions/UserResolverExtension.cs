﻿using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace ExpenseTracker.WebApi.Extensions
{
    public static class UserResolverExtension
    {
        public static int GetUserId(this HttpContext httpContext)
        {
            return int.Parse(httpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
        }
    }
}
