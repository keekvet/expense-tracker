﻿using AutoMapper;
using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Application.Services.Interfaces;
using ExpenseTracker.Domain.Models;
using ExpenseTracker.WebApi.Dto;
using ExpenseTracker.WebApi.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExpenseTracker.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/groups")]
    public class GroupController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGroupService _groupService;

        public GroupController(IMapper mapper, IGroupService groupService)
        {
            _mapper = mapper;
            _groupService = groupService;
        }

        [HttpPost]
        public async Task<IActionResult> AddGroupAsync([FromBody] GroupAddDto groupAddDto)
        {
            var group = await _groupService.AddGroupAsync(_mapper.Map<Group>(groupAddDto), HttpContext.GetUserId());

            return Ok(_mapper.Map<GroupDto>(group));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetGroupAsync(int id)
        {
            var group = await _groupService.GetGroupByUserAsync(id, HttpContext.GetUserId());

            if (group is null)
            { 
                return Forbid(); 
            }

            return Ok(_mapper.Map<GroupDto>(group));
        }

        [HttpGet("/api/users/{id}/groups")]
        public async Task<IActionResult> GetAllByUserAsync(int id, [FromQuery] PaginationModel paginationDto)
        {
            if (id != HttpContext.GetUserId())
            {
                return Forbid();
            }

            var groups = await _groupService.GetAllByUserAsync(id, paginationDto);

            return Ok(_mapper.Map<List<GroupDto>>(groups));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateGroupAsync([FromBody] GroupUpdateDto groupUpdateDto)
        {
            if (!await _groupService.CheckGroupContainsUserAsync(groupUpdateDto.Id, HttpContext.GetUserId()))
            {
                return Forbid();
            }

            await _groupService.UpdateGroupAsync(_mapper.Map<Group>(groupUpdateDto));

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGroupAsync(int id)
        {
            if (!await _groupService.CheckGroupContainsUserAsync(id, HttpContext.GetUserId()))
            {
                return Forbid();
            }

            await _groupService.DeleteGroupAsync(id);

            return NoContent();
        }

        [HttpPost("{groupId}/users/{userId}")]
        public async Task<IActionResult> AddUserToGroupAsync(int groupId, int userId)
        {
            if (!await _groupService.CheckGroupContainsUserAsync(groupId, HttpContext.GetUserId()))
            {
                return Forbid();
            }

            await _groupService.AddUserToGroupAsync(userId, groupId);
            
            return NoContent();
        }

        [HttpDelete("{groupId}/users/{userId}")]
        public async Task<IActionResult> RemoveUserFromGroupAsync(int groupId, int userId)
        {
            if (!await _groupService.CheckGroupContainsUserAsync(groupId, HttpContext.GetUserId()))
            {
                return Forbid();
            }

            await _groupService.RemoveUserFromGroupAsync(userId, groupId);

            return NoContent();
        }
    }
}
