﻿using AutoMapper;
using ExpenseTracker.Application.Models;
using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Application.Services.Interfaces;
using ExpenseTracker.Domain.Models;
using ExpenseTracker.WebApi.Dto;
using ExpenseTracker.WebApi.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExpenseTracker.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/sub-expenses")]
    public class SubExpenseController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IExpenseService _expenseService;
        private readonly ISubExpenseService _subExpenseService;

        public SubExpenseController(
            IMapper mapper,
            IExpenseService expenseService,
            ISubExpenseService subExpenseService)
        {
            _mapper = mapper;
            _expenseService = expenseService;
            _subExpenseService = subExpenseService;
        }

        [HttpPost]
        public async Task<IActionResult> AddSubExpenseAsync([FromBody] SubExpenseAddDto subExpenseAddDto)
        {
            if(!await _expenseService.CheckExpenseInSameGroupWithUserAsync(subExpenseAddDto.ExpenseId,
                                                                           subExpenseAddDto.UserId))
            {
                return Forbid();
            }

            var subExpense = await _subExpenseService.AddSubExpenseAsync(_mapper.Map<SubExpense>(subExpenseAddDto));

            return Ok(_mapper.Map<SubExpenseDto>(subExpense));
        }

        [HttpPost("range")]
        public async Task<IActionResult> ReplaceAsync([FromBody] SubExpenseReplaceManyDto subExpenseReplaceManyDto)
        {
            if (!await _expenseService
                .CheckExpenseInSameGroupWithUserAsync(subExpenseReplaceManyDto.ExpenseId, HttpContext.GetUserId()))
            {
                return Forbid();
            }

            await _subExpenseService.ReplaceInExpenseAsync(
                subExpenseReplaceManyDto.ExpenseId,
                _mapper.Map<List<SubExpense>>(subExpenseReplaceManyDto.SubExpenses));

            return NoContent();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSubExpenseAsync(int id)
        {
            if (!await _subExpenseService.CheckSubExpenseInSameGroupWithUserAsync(id, HttpContext.GetUserId()))
            {
                return Forbid();
            }

            var subExpense = await _subExpenseService.GetSubExpenseAsync(id);

            return Ok(_mapper.Map<SubExpenseDto>(subExpense));
        }

        [HttpGet("/api/users/{userId}/sub-expenses")]
        public async Task<IActionResult> GetAllByUserAsync(int userId, [FromQuery] PaginationModel paginationDto)
        {
            if (HttpContext.GetUserId() != userId)
            {
                return Forbid();
            }

            var subExpenses = await _subExpenseService.GetAllByUserAsync(userId, paginationDto);

            return Ok(_mapper.Map<List<SubExpenseDto>>(subExpenses));
        }

        [HttpGet("/api/expenses/{expenseId}/sub-expenses")]
        public async Task<IActionResult> GetAllByExpenseAsync(int expenseId, [FromQuery] PaginationModel paginationDto)
        {
            if (!await _expenseService.CheckExpenseInSameGroupWithUserAsync(expenseId, HttpContext.GetUserId()))
            {
                return Forbid();
            }

            var subExpenses = await _subExpenseService.GetAllByExpenseAsync(expenseId, paginationDto);

            return Ok(_mapper.Map<List<SubExpenseDto>>(subExpenses));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateSubExpenseAsync([FromBody] SubExpenseUpdateDto subExpenseUpdateDto)
        {
            if(!await _subExpenseService.CheckSubExpenseInSameGroupWithUserAsync(subExpenseUpdateDto.Id,
                                                                           HttpContext.GetUserId()))
            {
                return Forbid();
            }

            await _subExpenseService.UpdateSubExpenseAsync(_mapper.Map<SubExpense>(subExpenseUpdateDto));

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSubExpenseAsync(int id)
        {
            if (!await _subExpenseService.CheckSubExpenseInSameGroupWithUserAsync(id, HttpContext.GetUserId()))
            {
                return Forbid();
            }

            await _subExpenseService.DeleteSubExpenseAsync(id);

            return NoContent();
        }
    }
}
