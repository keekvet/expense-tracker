﻿using AutoMapper;
using ExpenseTracker.Application.Models;
using ExpenseTracker.Application.Services.Interfaces;
using ExpenseTracker.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ExpenseTracker.WebApi.Controllers
{
    [ApiController]
    [Route("api/registration")]
    public class RegistrationController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public RegistrationController(IMapper mapper, IUserService userService)
        {
            _mapper = mapper;
            _userService = userService;
        }

        [HttpPost]
        public async Task<IActionResult> RegisterAsync([FromBody] RegistrationDto registrationDto)
        {
            await _userService.AddUserAsync(_mapper.Map<RegistrationModel>(registrationDto));
            return NoContent();
        }
    }
}
