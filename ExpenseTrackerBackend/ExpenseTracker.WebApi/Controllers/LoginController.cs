﻿using AutoMapper;
using ExpenseTracker.Application.Services.Interfaces;
using ExpenseTracker.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ExpenseTracker.WebApi.Controllers
{
    [ApiController]
    [Route("api/login")]
    public class LoginController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ILoginService _loginService;

        public LoginController(IMapper mapper, ILoginService loginService)
        {
            _mapper = mapper;
            _loginService = loginService;
        }    

        [HttpPost]
        public async Task<IActionResult> LoginAsync([FromBody] LoginDto loginDto)
        {
            var loginModel = await _loginService.LoginAsync(loginDto.UserName, loginDto.Password);

            return Ok(_mapper.Map<UserTokenDto>(loginModel));
        }
    }
}
