﻿using AutoMapper;
using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Application.Services.Interfaces;
using ExpenseTracker.Domain.Models;
using ExpenseTracker.WebApi.Dto;
using ExpenseTracker.WebApi.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExpenseTracker.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/expenses")]
    public class ExpenseController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGroupService _groupService;
        private readonly IExpenseService _expenseService;

        public ExpenseController(
            IMapper mapper,
            IGroupService groupService,
            IExpenseService expenseService)
        {
            _mapper = mapper;
            _groupService = groupService;
            _expenseService = expenseService;
        }

        [HttpPost]
        public async Task<IActionResult> AddExpenseAsync([FromBody] ExpenseAddDto expenseAddDto)
        {
            if (!await _groupService.CheckGroupContainsUserAsync(expenseAddDto.GroupId, HttpContext.GetUserId()))
            {
                return Forbid();
            }

            var expense = await _expenseService.AddExpenseAsync(_mapper.Map<Expense>(expenseAddDto));

            return Ok(_mapper.Map<ExpenseDto>(expense));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetExpenseAsync(int id)
        {
            var expense = await _expenseService.GetExpenseAsync(id);

            if (expense is null 
                || !await _groupService.CheckGroupContainsUserAsync(expense.GroupId, HttpContext.GetUserId()))
            {
                return Forbid();
            }

            return Ok(_mapper.Map<ExpenseDto>(expense));
        }

        [HttpGet("/api/users/{userId}/expenses")]
        public async Task<IActionResult> GetAllByUserAsync(int userId, [FromQuery] PaginationModel paginationDto)
        {
            if (userId != HttpContext.GetUserId())
            {
                return Forbid();
            }

            var expenses = await _expenseService.GetAllByUserAsync(userId, paginationDto);

            return Ok(_mapper.Map<List<ExpenseDto>>(expenses));
        }

        [HttpGet("/api/groups/{groupId}/expenses")]
        public async Task<IActionResult> GetAllByGroupAsync(int groupId, [FromQuery] PaginationModel paginationModel)
        {
            if (!await _groupService.CheckGroupContainsUserAsync(groupId, HttpContext.GetUserId()))
            {
                return Forbid();
            }

            var expenses = await _expenseService.GetAllByGroupAsync(groupId, paginationModel);

            return Ok(_mapper.Map<List<ExpenseDto>>(expenses));
        }

        [HttpGet("{expenseId}/loans")]
        public async Task<IActionResult> GetAllLoansAsync(int expenseId)
        {
            if (!await _expenseService.CheckExpenseInSameGroupWithUserAsync(expenseId, HttpContext.GetUserId()))
            {
                return Forbid();
            }

            return Ok(await _expenseService.GetAllMoneyDifferencesAsync(expenseId));
        }


        [HttpGet("{expenseId}/users/{userId}/loans")]
        public async Task<IActionResult> GetUserLoanByExpenseAsync(int expenseId, int userId)
        {
            if (!await _expenseService.CheckExpenseInSameGroupWithUserAsync(expenseId, HttpContext.GetUserId()))
            {
                return Forbid();
            }

            return Ok(await _expenseService.GetUserMoneyDifferenceAsync(expenseId, userId));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateExpenseAsync([FromBody] ExpenseUpdateDto expenseUpdateDto)
        {
            if (!await _expenseService.CheckExpenseInSameGroupWithUserAsync(expenseUpdateDto.Id,
                                                                            HttpContext.GetUserId()))
            {
                return Forbid();
            }

            await _expenseService.UpdateExpenseAsync(_mapper.Map<Expense>(expenseUpdateDto));
            
             return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteExpenseAsync(int id)
        {
            if (!await _expenseService.CheckExpenseInSameGroupWithUserAsync(id, HttpContext.GetUserId()))
            {
                return Forbid();
            }

            await _expenseService.DeleteExpenseAsync(id);

            return NoContent();
        }
    }
}
