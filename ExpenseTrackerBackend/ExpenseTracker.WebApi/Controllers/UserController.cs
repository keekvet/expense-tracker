﻿using AutoMapper;
using ExpenseTracker.Application.Models;
using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Application.Services.Interfaces;
using ExpenseTracker.Domain.Models;
using ExpenseTracker.WebApi.Dto;
using ExpenseTracker.WebApi.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExpenseTracker.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/users")]
    public class UserController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IGroupService _groupService;

        public UserController(
            IMapper mapper,
            IUserService userService, 
            IGroupService groupService)
        {
            _mapper = mapper;
            _userService = userService;
            _groupService = groupService;
        }       
        
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUserAsync(int id)
        {
            var user = await _userService.GetUserAsync(id);
            return Ok(_mapper.Map<UserDto>(user));
        }

        [HttpGet("/api/groups/{groupId}/users")]
        public async Task<IActionResult> GetAllByGroupAsync(int groupId, [FromQuery] PaginationModel paginationModel)
        {
            if (!await _groupService.CheckGroupContainsUserAsync(groupId, HttpContext.GetUserId()))
            {
                return Forbid();
            }

            var users = await _userService.GetAllByGroupAsync(groupId, paginationModel);

            return Ok(_mapper.Map<List<UserDto>>(users));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateUserAsync([FromBody] UserUpdateDto userUpdateDto)
        {
            if (HttpContext.GetUserId() != userUpdateDto.Id)
            {
                return Forbid();
            }

            await _userService.UpdateUserAsync(_mapper.Map<User>(userUpdateDto));

            return NoContent();
        }

        [HttpPut("{id}/password")]
        public async Task<IActionResult> UpdatePasswordAsync(int id, [FromBody] UserPasswordUpdateDto userPasswordUpdateDto)
        {
            if (HttpContext.GetUserId() != id)
            {
                return Forbid();
            }

            var passwordUpdateModel = new UserPasswordUpdateModel()
            {
                UserId = id,
                OldPassword = userPasswordUpdateDto.OldPassword,
                NewPassword = userPasswordUpdateDto.NewPassword
            };

            await _userService.UpdatePasswordAsync(passwordUpdateModel);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserAsync(int id)
        {
            if (HttpContext.GetUserId() != id)
            {
                return Forbid();
            }

            await _userService.DeleteUserAsync(id);

            return NoContent();
        }
    }
}
