﻿using AutoMapper;
using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Application.Services.Interfaces;
using ExpenseTracker.Domain.Models;
using ExpenseTracker.WebApi.Dto;
using ExpenseTracker.WebApi.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExpenseTracker.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/expenses/comments")]
    public class ExpenseCommentController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IExpenseService _expenseService;
        private readonly IExpenseCommentService _expenseCommentService;

        public ExpenseCommentController(
            IMapper mapper,
            IExpenseService expenseService,
            IExpenseCommentService expenseCommentService)
        {
            _mapper = mapper;
            _expenseService = expenseService;
            _expenseCommentService = expenseCommentService;
        }

        [HttpPost("/api/expenses/{expenseId}/comments")]
        public async Task<IActionResult> AddExpenseCommentAsync(int expenseId, ExpenseCommentAddDto expenseCommentDto)
        {
            int userId = HttpContext.GetUserId();

            if (!await _expenseService.CheckExpenseInSameGroupWithUserAsync(expenseId, userId))
            {
                return Forbid();
            }

            var comment = new ExpenseComment()
            {
                Message = expenseCommentDto.Message,
                ExpenseId = expenseId,
                UserId = userId
            };

            comment = await _expenseCommentService.AddExpenseCommentAsync(comment);
            
            return Ok(_mapper.Map<ExpenseCommentDto>(comment));
        }

        [HttpGet("/api/expenses/{expenseId}/comments")]
        public async Task<IActionResult> GetAllByExpenseIdAsync(
            int expenseId, 
            [FromQuery] PaginationModel paginationModel)
        {
            if(!await _expenseService.CheckExpenseInSameGroupWithUserAsync(expenseId, HttpContext.GetUserId()))
            {
                return Forbid();
            }

            var commets = await _expenseCommentService.GetAllByExpenseIdAsync(expenseId, paginationModel);

            return Ok(_mapper.Map<IEnumerable<ExpenseCommentDto>>(commets));
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteExpenseCommentAsync(int id)
        {
            if (!await _expenseCommentService.CheckCommentBelongsToUserAsync(id, HttpContext.GetUserId()))
            {
                return Forbid();
            }

            await _expenseCommentService.DeleteExpenseCommentAsync(id);

            return NoContent();
        }
    }
}
