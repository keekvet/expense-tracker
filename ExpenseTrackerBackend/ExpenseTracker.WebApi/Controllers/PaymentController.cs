﻿using AutoMapper;
using ExpenseTracker.Application.Models.Pagination;
using ExpenseTracker.Application.Services.Interfaces;
using ExpenseTracker.Domain.Models;
using ExpenseTracker.WebApi.Dto;
using ExpenseTracker.WebApi.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExpenseTracker.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/payments")]
    public class PaymentController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IPaymentService _paymentService;

        public PaymentController(IMapper mapper, IPaymentService paymentService)
        {
            _mapper = mapper;
            _paymentService = paymentService;
        }

        [HttpPost]
        public async Task<IActionResult> AddPaymentAsync([FromBody] PaymentAddDto paymentAddDto)
        {
            if(paymentAddDto.SenderId != HttpContext.GetUserId())
            {
                return Forbid();
            }

            var payment = await _paymentService.AddPaymentAsync(_mapper.Map<Payment>(paymentAddDto));

            return Ok(_mapper.Map<PaymentDto>(payment));
        }
        
        [HttpGet("/api/users/{userId}/payments")]
        public async Task<IActionResult> GetPaymentsByUserAsync(int userId, [FromQuery] PaginationModel paginationModel)
        {
            if (userId != HttpContext.GetUserId())
            {
                return Forbid();
            }

            var payments = await _paymentService.GetPaymentsByUserAsync(userId, paginationModel);

            return Ok(_mapper.Map<List<PaymentDto>>(payments));
        }
    }
}
