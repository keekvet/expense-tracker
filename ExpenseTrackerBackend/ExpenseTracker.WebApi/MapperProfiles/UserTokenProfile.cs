﻿using AutoMapper;
using ExpenseTracker.Application.Models;
using ExpenseTracker.WebApi.Dto;

namespace ExpenseTracker.WebApi.MapperProfiles
{
    public class UserTokenProfile : Profile
    {
        public UserTokenProfile()
        {
            CreateMap<UserTokenModel, UserTokenDto>();
        }
    }
}
