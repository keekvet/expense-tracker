﻿using AutoMapper;
using ExpenseTracker.Application.Models;
using ExpenseTracker.WebApi.Dto;

namespace ExpenseTracker.WebApi.MapperProfiles
{
    public class RegistrationProfile : Profile
    {
        public RegistrationProfile()
        {
            CreateMap<RegistrationDto, RegistrationModel>();
        }
    }
}
