﻿using AutoMapper;
using ExpenseTracker.Domain.Models;
using ExpenseTracker.WebApi.Dto;

namespace ExpenseTracker.WebApi.MapperProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, User>()
                .ForMember(u => u.Salt, opt => opt.Ignore())
                .ForMember(u => u.PasswordHash, opt => opt.Ignore());

            CreateMap<User, UserDto>().ReverseMap();

            CreateMap<UserUpdateDto, User>();
        }
    }
}
