﻿using AutoMapper;
using ExpenseTracker.Domain.Models;
using ExpenseTracker.WebApi.Dto;

namespace ExpenseTracker.WebApi.MapperProfiles
{
    public class ExpenseProfile : Profile
    {
        public ExpenseProfile()
        {
            CreateMap<ExpenseAddDto, Expense>();
            CreateMap<ExpenseUpdateDto, Expense>();

            CreateMap<Expense, ExpenseDto>();
        }        
    }
}
