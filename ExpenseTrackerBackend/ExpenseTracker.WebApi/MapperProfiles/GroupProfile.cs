﻿using AutoMapper;
using ExpenseTracker.Domain.Models;
using ExpenseTracker.WebApi.Dto;

namespace ExpenseTracker.WebApi.MapperProfiles
{
    public class GroupProfile : Profile
    {
        public GroupProfile()
        {
            CreateMap<GroupAddDto, Group>();
            CreateMap<GroupUpdateDto, Group>();

            CreateMap<Group, GroupDto>();
        }
    }
}
