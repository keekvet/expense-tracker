﻿using AutoMapper;
using ExpenseTracker.Domain.Models;
using ExpenseTracker.WebApi.Dto;

namespace ExpenseTracker.WebApi.MapperProfiles
{
    public class PaymentProfile : Profile
    {
        public PaymentProfile()
        {
            CreateMap<PaymentAddDto, Payment>();

            CreateMap<Payment, PaymentDto>();
        }
    }
}
