﻿using AutoMapper;
using ExpenseTracker.Domain.Models;
using ExpenseTracker.WebApi.Dto;

namespace ExpenseTracker.WebApi.MapperProfiles
{
    public class SubExpenseProfile : Profile
    {
        public SubExpenseProfile()
        {
            CreateMap<SubExpenseAddDto, SubExpense>();
            CreateMap<SubExpenseUpdateDto, SubExpense>();

            CreateMap<SubExpenseReplaceDto, SubExpense>();

            CreateMap<SubExpense, SubExpenseDto>();
        }
    }
}
