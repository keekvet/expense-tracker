﻿using AutoMapper;
using ExpenseTracker.Domain.Models;
using ExpenseTracker.WebApi.Dto;

namespace ExpenseTracker.WebApi.MapperProfiles
{
    public class ExpenseCommentProfile : Profile
    {
        public ExpenseCommentProfile()
        {
            CreateMap<ExpenseComment, ExpenseCommentDto>();
        }
    }
}
