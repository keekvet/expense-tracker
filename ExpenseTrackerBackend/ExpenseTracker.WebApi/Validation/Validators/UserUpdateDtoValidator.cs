﻿using ExpenseTracker.WebApi.Dto;
using FluentValidation;

namespace ExpenseTracker.WebApi.Validation.Validators
{
    public class UserUpdateDtoValidator : AbstractValidator<UserUpdateDto>
    {
        public UserUpdateDtoValidator()
        {
            RuleFor(u => u.Name).UserNameTemplate();
        }
    }
}
