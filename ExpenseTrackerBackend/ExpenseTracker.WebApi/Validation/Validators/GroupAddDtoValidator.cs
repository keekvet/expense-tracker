﻿using ExpenseTracker.WebApi.Dto;
using FluentValidation;

namespace ExpenseTracker.WebApi.Validation.Validators
{
    public class GroupAddDtoValidator : AbstractValidator<GroupAddDto>
    {
        public GroupAddDtoValidator()
        {
            RuleFor(g => g.Name).NotEmpty();
        }
    }
}
