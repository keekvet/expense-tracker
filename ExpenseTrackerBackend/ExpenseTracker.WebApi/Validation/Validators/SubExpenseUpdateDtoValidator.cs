﻿using ExpenseTracker.WebApi.Dto;
using FluentValidation;

namespace ExpenseTracker.WebApi.Validation.Validators
{
    public class SubExpenseUpdateDtoValidator : AbstractValidator<SubExpenseUpdateDto>
    {
        public SubExpenseUpdateDtoValidator()
        {
            RuleFor(s => s.Id).GreaterThan(0);
            RuleFor(s => s.SplitValue).GreaterThan(0);
        }
    }
}
