﻿using ExpenseTracker.WebApi.Dto;
using FluentValidation;

namespace ExpenseTracker.WebApi.Validation.Validators
{
    public class RegistrationDtoValidator : AbstractValidator<RegistrationDto>
    {
        public RegistrationDtoValidator()
        {
            RuleFor(r => r.UserName).UserNameTemplate();
            RuleFor(r => r.Password).PasswordTemplate();
            RuleFor(r => r.PasswordConfirmation).Equal(r => r.Password);
        }
    }
}
