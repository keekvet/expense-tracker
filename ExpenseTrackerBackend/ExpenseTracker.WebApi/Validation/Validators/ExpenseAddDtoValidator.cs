﻿using ExpenseTracker.WebApi.Dto;
using FluentValidation;

namespace ExpenseTracker.WebApi.Validation.Validators
{
    public class ExpenseAddDtoValidator : AbstractValidator<ExpenseAddDto>
    {
        public ExpenseAddDtoValidator()
        {
            RuleFor(e => e.Name).NotEmpty();
            RuleFor(e => e.SplitType).IsInEnum();
        }
    }
}
