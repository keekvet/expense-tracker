﻿using ExpenseTracker.Application.ConfigurationSections;
using ExpenseTracker.Application.Models.Pagination;
using FluentValidation;
using Microsoft.Extensions.Options;

namespace ExpenseTracker.WebApi.Validation.Validators
{
    public class PaginationDtoValidator : AbstractValidator<PaginationModel>
    {
        public PaginationDtoValidator(IOptions<PaginationConfiguration> pagintionConfig)
        {
            RuleFor(p => p.Count).InclusiveBetween(0, pagintionConfig.Value.MaxItemsPerPage);
            RuleFor(p => p.Page).GreaterThan(0);
        }
    }
}
