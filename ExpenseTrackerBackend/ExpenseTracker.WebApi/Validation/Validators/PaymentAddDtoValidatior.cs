﻿using ExpenseTracker.WebApi.Dto;
using FluentValidation;

namespace ExpenseTracker.WebApi.Validation.Validators
{
    public class PaymentAddDtoValidatior : AbstractValidator<PaymentAddDto>
    {
        public PaymentAddDtoValidatior()
        {
            RuleFor(p => p.SenderId).NotEmpty();
            RuleFor(p => p.ReceiverId).NotEmpty();

            RuleFor(p => p.Amount).GreaterThan(0);
            RuleFor(p => p.Note).MaximumLength(500);
        }
    }
}
