﻿using ExpenseTracker.WebApi.Dto;
using FluentValidation;

namespace ExpenseTracker.WebApi.Validation.Validators
{
    public class SubExpenseReplaceManyDtoValidator : AbstractValidator<SubExpenseReplaceManyDto>
    {
        public SubExpenseReplaceManyDtoValidator()
        {
            RuleFor(s => s.ExpenseId).GreaterThan(0);
        }
    }
}
