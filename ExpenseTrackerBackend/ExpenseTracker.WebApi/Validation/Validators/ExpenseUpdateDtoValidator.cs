﻿using ExpenseTracker.WebApi.Dto;
using FluentValidation;

namespace ExpenseTracker.WebApi.Validation.Validators
{
    public class ExpenseUpdateDtoValidator : AbstractValidator<ExpenseUpdateDto>
    {
        public ExpenseUpdateDtoValidator()
        {
            RuleFor(e => e.Name).NotEmpty();
            RuleFor(e => e.SplitType).IsInEnum();
        }        
    }
}
