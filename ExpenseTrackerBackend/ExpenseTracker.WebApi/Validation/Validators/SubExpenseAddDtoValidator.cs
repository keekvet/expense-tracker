﻿using ExpenseTracker.WebApi.Dto;
using FluentValidation;

namespace ExpenseTracker.WebApi.Validation.Validators
{
    public class SubExpenseAddDtoValidator : AbstractValidator<SubExpenseAddDto>
    {
        public SubExpenseAddDtoValidator()
        {
            RuleFor(s => s.SplitValue).GreaterThan(0);

            RuleFor(s => s.ExpenseId).NotEmpty();
            RuleFor(s => s.UserId).NotEmpty();
        }
    }
}
