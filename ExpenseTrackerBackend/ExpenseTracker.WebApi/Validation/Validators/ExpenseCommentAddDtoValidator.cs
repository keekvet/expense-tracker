﻿using ExpenseTracker.WebApi.Dto;
using FluentValidation;

namespace ExpenseTracker.WebApi.Validation.Validators
{
    public class ExpenseCommentAddDtoValidator : AbstractValidator<ExpenseCommentAddDto>
    {
        public ExpenseCommentAddDtoValidator()
        {
            RuleFor(e => e.Message).NotEmpty().MaximumLength(500);
        }
    }
}
