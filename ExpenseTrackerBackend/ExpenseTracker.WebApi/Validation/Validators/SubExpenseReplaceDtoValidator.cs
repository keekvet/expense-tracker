﻿using ExpenseTracker.WebApi.Dto;
using FluentValidation;

namespace ExpenseTracker.WebApi.Validation.Validators
{
    public class SubExpenseReplaceDtoValidator : AbstractValidator<SubExpenseReplaceDto>
    {
        public SubExpenseReplaceDtoValidator()
        {
            RuleFor(s => s.UserId).NotEmpty();
            RuleFor(s => s.SplitValue).GreaterThan(0);
        }
    }
}
