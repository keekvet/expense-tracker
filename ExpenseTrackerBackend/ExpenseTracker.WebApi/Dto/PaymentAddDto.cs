﻿namespace ExpenseTracker.WebApi.Dto
{
    public class PaymentAddDto
    {
        public string Note { get; set; }
        public decimal Amount { get; set; }

        public int SenderId { get; set; }

        public int ReceiverId { get; set; }

        public int ExpenseId { get; set; }
    }
}
