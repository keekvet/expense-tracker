﻿using System;

namespace ExpenseTracker.WebApi.Dto
{
    public class PaymentDto
    {
        public int Id { get; set; }
        public string Note { get; set; }
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
        public bool IsAccepted { get; set; }

        public int SenderId { get; set; }

        public int ReceiverId { get; set; }

        public int ExpenseId { get; set; }

    }
}
