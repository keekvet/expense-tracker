﻿using ExpenseTracker.Domain.Enums;

namespace ExpenseTracker.WebApi.Dto
{
    public class ExpenseAddDto
    {
        public string Name { get; set; }
        public int GroupId { get; set; }
        public ExpenseSplitType SplitType { get; set; }
    }
}
