﻿namespace ExpenseTracker.WebApi.Dto
{
    public class GroupUpdateDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
