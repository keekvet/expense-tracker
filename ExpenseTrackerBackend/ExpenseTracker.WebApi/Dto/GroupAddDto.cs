﻿namespace ExpenseTracker.WebApi.Dto
{
    public class GroupAddDto
    {
        public string Name { get; set; }
    }
}
