﻿namespace ExpenseTracker.WebApi.Dto
{
    public class UserTokenDto
    {
        public string Token { get; set; }
        public UserDto User { get; set; }

    }
}
