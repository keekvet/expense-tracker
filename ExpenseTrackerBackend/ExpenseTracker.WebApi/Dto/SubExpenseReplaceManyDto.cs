﻿using System.Collections.Generic;

namespace ExpenseTracker.WebApi.Dto
{
    public class SubExpenseReplaceManyDto
    {
        public int ExpenseId { get; set; }

        public ICollection<SubExpenseReplaceDto> SubExpenses { get; set; }
    }
}
