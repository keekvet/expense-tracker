﻿namespace ExpenseTracker.WebApi.Dto
{
    public class RegistrationDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string PasswordConfirmation { get; set; }
    }
}
