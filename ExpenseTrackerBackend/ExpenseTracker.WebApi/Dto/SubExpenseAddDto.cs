﻿namespace ExpenseTracker.WebApi.Dto
{
    public class SubExpenseAddDto
    {
        public decimal SplitValue { get; set; }
        public decimal SpentAmount { get; set; }

        public int ExpenseId { get; set; }

        public int UserId { get; set; }
    }
}
