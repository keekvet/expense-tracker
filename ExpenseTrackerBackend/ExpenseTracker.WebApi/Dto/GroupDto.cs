﻿using System.Collections.Generic;

namespace ExpenseTracker.WebApi.Dto
{
    public class GroupDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<UserDto> Users { get; set; }
    }
}
