﻿using ExpenseTracker.Domain.Enums;

namespace ExpenseTracker.WebApi.Dto
{
    public class ExpenseUpdateDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ExpenseSplitType SplitType { get; set; }
    }
}
