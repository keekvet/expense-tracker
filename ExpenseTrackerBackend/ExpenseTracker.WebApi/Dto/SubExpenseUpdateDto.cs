﻿namespace ExpenseTracker.WebApi.Dto
{
    public class SubExpenseUpdateDto
    {
        public int Id { get; set; }
        public decimal SplitValue { get; set; }
        public decimal SpentAmount { get; set; }
    }
}
