﻿using ExpenseTracker.Domain.Enums;
using System;

namespace ExpenseTracker.WebApi.Dto
{
    public class ExpenseDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public DateTime Date { get; set; }
        public ExpenseSplitType SplitType { get; set; }

        public int GroupId { get; set; }


    }
}
