﻿using System;

namespace ExpenseTracker.WebApi.Dto
{
    public class ExpenseCommentDto
    {

        public int Id { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }

        public int ExpenseId { get; set; }

        public int UserId { get; set; }
    }
}
