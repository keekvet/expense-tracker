﻿namespace ExpenseTracker.WebApi.Dto
{
    public class UserUpdateDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
