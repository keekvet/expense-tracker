﻿namespace ExpenseTracker.WebApi.Dto
{
    public class ErrorDto
    {
        public string Type { get; set; }
        public object Message { get; set; }
    }
}
