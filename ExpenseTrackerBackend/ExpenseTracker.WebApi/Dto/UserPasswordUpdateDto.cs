﻿namespace ExpenseTracker.WebApi.Dto
{
    public class UserPasswordUpdateDto
    {
        public string NewPassword { get; set; }
        public string OldPassword { get; set; }
    }
}
