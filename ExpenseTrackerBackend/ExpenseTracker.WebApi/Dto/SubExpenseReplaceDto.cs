﻿
namespace ExpenseTracker.WebApi.Dto
{
    public class SubExpenseReplaceDto
    {
        public decimal SplitValue { get; set; }
        public decimal SpentAmount { get; set; }

        public int UserId { get; set; }
    }
}
