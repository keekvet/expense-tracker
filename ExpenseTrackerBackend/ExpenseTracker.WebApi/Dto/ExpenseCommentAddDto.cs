﻿namespace ExpenseTracker.WebApi.Dto
{
    public class ExpenseCommentAddDto
    {
        public string Message { get; set; }
    }
}
